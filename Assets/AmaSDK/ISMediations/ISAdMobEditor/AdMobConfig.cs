using UnityEngine;

[CreateAssetMenu(fileName = "AdMobConfig", menuName = "Amanotes/Ironsrc/Mediations/Create AdMobConfig", order = 0)]
public class AdMobConfig : ScriptableObject {
    
    [Tooltip("Android: AdMob’s SDK requires publishers to add their AdMob App ID to the app’s AndroidManifest as a child of the <application></application> tag")]
    public string AppID;

    [Tooltip("iOS: AdMob’s SDK requires publishers to add their AdMob App ID to the app’s plist ")]
    public string GADApplicationIdentifier;
}