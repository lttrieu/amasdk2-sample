﻿
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using System.Linq;
using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Ironsrc;

namespace Amanotes.TripleSDK.Ironsrc.Mediations
{
    public class AdMobBuildProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }

        public void OnPreprocessBuild(BuildReport report)
        {
            if (report.summary.platform == BuildTarget.Android)
            {
                UpdateAndroidManifest();
            }
        }

        [MenuItem("Amanotes/IronSource/AdMob/Update AndroidManifest")]
        private static void UpdateAndroidManifest()
        {

            XmlDocument root = AndroidManifestHelper.LoadXmlDocument(IronSourceEditor.ANDROID_MANIFEST);
            if (root != null)
            {
               
                AndroidManifestHelper.AddActivity(
                    root,
                    "com.google.android.gms.ads.AdActivity",
                    new System.Collections.Generic.Dictionary<string, string>(){
                        {"configChanges", "keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"},
                        {"theme", "@android:style/Theme.Translucent"}
                    });
                AndroidManifestHelper.SaveXml(root, IronSourceEditor.ANDROID_MANIFEST);

                UnityEngine.Debug.Log($"AdMob prebuild processing is done.");
            }

        }
        [PostProcessBuildAttribute(1)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string pathToBuiltProject)
        {

        }
    }

}
