using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

namespace Amanotes.TripleSDK.SDKSetupWizard
{
    [InitializeOnLoad]
    static internal class PackageHelper
    {
        private static Type _pm;

        public static Type PM
        {
            get
            {
                if (_pm == null)
                {
                    _pm = Utils.FindClass("Amanotes", "Amanotes.PackageManager.PackageManager");
                }
                return _pm;
            }
        }

        private static Type _ConfigurationBuilder;
        public static Type UnityIAPConfigurationBuilder
        {
            get
            {
                if (_ConfigurationBuilder == null)
                {
                    _ConfigurationBuilder = Utils.FindClass("UnityEngine.Purchasing", "UnityEngine.Purchasing.ConfigurationBuilder");
                }
                return _ConfigurationBuilder;
            }
        }

        private static Type _GenerateXmlFromGoogleServicesJson;
        public static Type GenerateXmlFromGoogleServicesJson
        {
           get {
                if (_GenerateXmlFromGoogleServicesJson == null)
                {
                    _GenerateXmlFromGoogleServicesJson = Utils.FindClass(null, "Firebase.Editor.GenerateXmlFromGoogleServicesJson");
                }
                return _GenerateXmlFromGoogleServicesJson;
            }
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            StaticInit();  
        }

        public static string PackageListFile
        {
            get
            {
                return Path.Combine(Application.persistentDataPath, "list.json");
            }
        }

        public static Package[] Packages { get; private set; }

        public static PackageList AllPackages { get; private set; }

        private static void SaveJson(object obj, string path)
        {
            var json = JsonUtility.ToJson(obj, true);
            File.WriteAllText(path, json);
        }

        public static PackageList LoadPackageList(string path)
        {
            PackageList packageList = null;

            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);

                packageList = JsonUtility.FromJson<PackageList>(json);
            }
            if (packageList == null)
            {
                packageList = new PackageList()
                {
                    Packages = new Package[0]
                };
            }
            else if (packageList.Packages == null)
            {
                packageList.Packages = new Package[0];
            }

            return packageList;
        }


        private static PackageList m_installedPackages;

        static PackageHelper()
        {
            StaticInit();
        }

        public static void StaticInit()
        {
            Packages = new Package[] {
         
                new Package() {Name = "IronSourceAdColonyAdapter", Version = "4.1.11", DefaultCheck = false},
                new Package() {Name = "ISAdMobEditor", Version = "3", DefaultCheck = true, Interactive = false},
                new Package() {Name = "IronSourceAmazonAdapter", Version = "4.3.2", DefaultCheck = false},
                new Package() {Name = "IronSourceAppLovinAdapter", Version = "4.3.10", DefaultCheck = false},
                new Package() {Name = "IronSourceMintegralAdapter", Version = "4.1.4", DefaultCheck = false},
                new Package() {Name = "IronSourceUnityAdsAdapter", Version = "4.1.9", DefaultCheck = false},
                new Package() {Name = "IronSourceFacebookAdapter", Version = "4.3.16", DefaultCheck = false},
            
                new Package() {Name = "AmaAppsflyers", Version = "7"},
                new Package() {Name = "AmaIronSource", Version = "16"},
                new Package() {Name = "AmaFirebase_Dotnet4", Version = "8"},
                new Package() {Name = "AmaSDKUnityIAP", Version = "5"},
                new Package() {Name = "IronSourceTikTokAdapter", Version = "4.1.2", DefaultCheck = false},
            };
            m_installedPackages = LoadPackageList(Path.Combine(Application.dataPath, "package.json"));
            AllPackages = LoadPackageList(PackageListFile);
        }

        public static List<Package> FindPackages(string package)
        {
            return (from e in AllPackages.Packages
                    where e.Name.Equals(package, StringComparison.OrdinalIgnoreCase)
                    select e).ToList();
        }

         public static List<Package> FindInstalledPackages(string package)
        {
            return (from e in m_installedPackages.Packages
                    where e.Name.Equals(package, StringComparison.OrdinalIgnoreCase)
                    select e).ToList();
        }

        public static void AddPackage(string package, string version)
        {
            var packages = new List<Package>(m_installedPackages.Packages);
            var index = packages.FindIndex(e => e.Name == package);
            if (index >= 0)
            {
                packages[index].Version = version;
            }
            else
            {
                var item = AllPackages.Packages.FirstOrDefault(p => p.Name == package && p.Version == version);
                if (item != null)
                {
                    packages.Add(item);
                }
                else
                {
                    Debug.LogError($"Not found package {package}");
                }
            }

            m_installedPackages.Packages = packages.ToArray();
        }

        public static bool IsPackageInstalled(string package)
        {
            return m_installedPackages.Packages.Any(p => p.Name == package);
        }

        public static void LoadPackages()
        {
            m_installedPackages = LoadPackageList(Path.Combine(Application.dataPath, "package.json"));
        }

        public static void SavePackages()
        {
            SaveJson(m_installedPackages, Path.Combine(Application.dataPath, "package.json"));
        }

        public static void RemovePackage(string packageName)
        {
            if(PM != null) {
                Utils.InvokeStaticMethod(PM, "RemovePackage", new object[]{
                    packageName
                });
            } else {
                EditorUtility.DisplayDialog("Error", $"Package {packageName} not found", "OK");
            }
        }
    }
}
