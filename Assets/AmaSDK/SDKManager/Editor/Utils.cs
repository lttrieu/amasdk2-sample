using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Amanotes.TripleSDK.SDKSetupWizard
{
    public static class Utils
    {
        public static Type FindClass(string assemblyName, string className)
        {
            bool flag = !string.IsNullOrEmpty(assemblyName);
            string text = (!flag) ? className : (className + ", " + assemblyName);
            Type type = Type.GetType(text);
            if (type == null)
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                for (int i = 0; i < assemblies.Length; i++)
                {
                    Assembly assembly = assemblies[i];
                    if (flag)
                    {
                        if (assembly.GetName().Name == assemblyName)
                        {
                            type = Type.GetType(className + ", " + assembly.FullName);
                            break;
                        }
                    }
                    else
                    {
                        Type[] types = assembly.GetTypes();
                        for (int j = 0; j < types.Length; j++)
                        {
                            Type type2 = types[j];
                            if (type2.FullName == className)
                            {
                                type = type2;
                            }
                        }
                        if (type != null)
                        {
                            break;
                        }
                    }
                }
            }

            return type;
        }

        public static Type FindClass(string className)
        {
            return FindClass(null, className);
        }

        public static Type VersionHandlerImpl
        {
            get
            {
                return FindClass("Google.VersionHandlerImpl", "Google.VersionHandlerImpl");
            }
        }

        public static Type GooglePlayServicesImpl
        {
            get
            {
                return FindClass("Google.JarResolver", "GooglePlayServices.PlayServicesResolver");
            }
        }

        public static Type GoogleResolverSettingDialog
        {
            get
            {
                return FindClass("Google.JarResolver", "GooglePlayServices.SettingsDialog");
            }
        }

        public static void SetNonPublicStaticProperty (Type type, string name, object value) 
        {
            var property = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Static);
            property.SetValue(type, value);
        }

        public static void GetNonPublicStaticField (Type type, string name, object value) 
        {
            var property = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Static);
            property.SetValue(type, value);
        }

        public static Type AndroidManifestHelper  {
             get
            {
                return FindClass(null, "Amanotes.TripleSDK.Core.AndroidManifestHelper");
            }
        }
        public static object InvokeMethod(Type type, object objectInstance, string methodName, object[] args, Dictionary<string, object> namedArgs = null)
        {
            Type[] array = null;
            if (args != null && args.Length > 0)
            {
                array = new Type[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    array[i] = args[i].GetType();
                }
            }
            MethodInfo methodInfo = (array == null) ? type.GetMethod(methodName) : type.GetMethod(methodName, array);
            ParameterInfo[] parameters = methodInfo.GetParameters();
            int num = parameters.Length;
            object[] array2 = new object[num];
            int num2 = (args == null) ? 0 : args.Length;
            ParameterInfo[] array3 = parameters;
            for (int j = 0; j < array3.Length; j++)
            {
                ParameterInfo parameterInfo = array3[j];
                int position = parameterInfo.Position;
                if (position < num2)
                {
                    array2[position] = args[position];
                }
                else
                {
                    object obj = parameterInfo.RawDefaultValue;
                    object obj2;
                    if (namedArgs != null && namedArgs.TryGetValue(parameterInfo.Name, out obj2))
                    {
                        obj = obj2;
                    }
                    array2[position] = obj;
                }
            }
            return methodInfo.Invoke(objectInstance, array2);
        }

        public static object InvokeStaticMethod(Type type, string methodName, object[] args, Dictionary<string, object> namedArgs = null)
        {
            return InvokeMethod(type, null, methodName, args, namedArgs);
        }

        
        public static object InvokeGooglePlayServicesMethod(string methodName, object[] args = null,
                   Dictionary<string, object> namedArgs = null)
        {
            Type serviceIml = GooglePlayServicesImpl;

            if (serviceIml == null)
            {
                DisplayMissingFirebaseInstalation();
                return null;
            }
            return InvokeStaticMethod(serviceIml, methodName, args, namedArgs);
        }

        private static void DisplayMissingFirebaseInstalation()
        {
            EditorUtility.DisplayDialog("Google Version Handler Error",
            @"Goolge Version Handler is not available.
            Have you added AmaFirebase package?
            (Try to restart UnityEditor if you install Firebase for the first time).", 
            "OK");
            FixGoogleServiceResolver();
        }

        public static void FixGoogleServiceResolver()
        {
            string[] files = new string[] {
                "Google.IOSResolver",
                "Google.JarResolver",
                "Google.VersionHandler",
                "Google.VersionHandlerImpl",
                "Firebase.Editor",
               "Firebase.Crashlytics.Editor",
            };
            foreach(var file in files) {
                FixEditorPlugin(file);
            }
        }

        private static void FixEditorPlugin(string assetName)
        {
             // AssetDatabase.LoadAssetAtPath("Assets/PlayServicesResolver/Editor/")
            var assets = AssetDatabase.FindAssets(assetName);
            foreach(var uuid in assets) {
                // Debug.Log("AA" + item.GetType().Name);
                var path = AssetDatabase.GUIDToAssetPath(uuid);
                PluginImporter importer = AssetImporter.GetAtPath (path) as PluginImporter;
                // var aaset = AssetDatabase.LoadAssetAtPath<PluginImporter>(path);
                if(importer != null) {
                    Debug.Log(path);
                    importer.SetCompatibleWithEditor(true);
                    importer.SaveAndReimport();
                }
            }
        }

        public static object InvokeVersionHandlerMethod(string methodName, object[] args = null,
                    Dictionary<string, object> namedArgs = null)
        {
            var impl = VersionHandlerImpl;
            if (impl == null)
            {
                DisplayMissingFirebaseInstalation();
                return null;
            }
            return InvokeStaticMethod(impl, methodName, args, namedArgs);

        }
    }

}