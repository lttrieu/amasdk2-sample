using System;
using System.IO;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Amanotes.TripleSDK.SDKSetupWizard
{
    [Serializable]
    public class Package
    {
        public string Name;
        public string Version;
        public bool DefaultCheck = true;
        public bool Interactive = true;
        private int? versionCode;
        public string GetPrefName()
        {
            return $"AMA_FWW_{Name}";
        }

        public int GetVersionCode() 
        {
            if (versionCode == null)
            {
                var outVal = 0;
                int.TryParse(Version.Replace(".", ""), out outVal);
                versionCode = outVal;
            }
            return versionCode.Value;
        }
    }


    public class PackageList
    {
        public Package[] Packages;
    }


    internal class Configuration
    {
        public Package[] Packages;

        public string refPackageManager = "https://docs.google.com/document/d/1aBrvkn8UMWYAbZRXCDAQg4-TaAQcpkP_muGgEsAP2GA/edit#heading=h.g74p8ybf8mz6";

        public string refFirebase = "https://docs.google.com/document/d/1aBrvkn8UMWYAbZRXCDAQg4-TaAQcpkP_muGgEsAP2GA/edit#heading=h.i0havwpsj15w";
        public string refIronSource = "https://docs.google.com/document/d/1aBrvkn8UMWYAbZRXCDAQg4-TaAQcpkP_muGgEsAP2GA/edit#heading=h.j9cv4bytszyj";
        public string refIAP = "https://docs.google.com/document/d/1aBrvkn8UMWYAbZRXCDAQg4-TaAQcpkP_muGgEsAP2GA/edit#heading=h.j9q3zkwcdlmu";
        public string refAppsflyer = "https://docs.google.com/document/d/1aBrvkn8UMWYAbZRXCDAQg4-TaAQcpkP_muGgEsAP2GA/edit#heading=h.5rz7k3q80qj";

        public Configuration()
        {
            Packages = PackageHelper.Packages;

            m_PackageSelectCache = new Dictionary<string, int>();
            foreach (var p in Packages)
            {
                var select = PlayerPrefs.GetInt(p.GetPrefName(), -1);
                if (select == -1) {
                    select = p.DefaultCheck ? 1 : 0;
                }
                m_PackageSelectCache[p.Name] = select;
            }
        }

        public bool Interactive(string name) {
            var pkg = this.Packages.FirstOrDefault(e => e.Name == name);
            return pkg != null ? pkg.Interactive : false;
        }

        private Dictionary<string, int> m_PackageSelectCache;

        public string IronSourcePackage = "AmaIronSource";

        public string AppsflyerPackage = "AmaAppsflyers";

        public string AmaUnityIAP = "AmaSDKUnityIAP";

        public string FirebasePackage = "AmaFirebase_Dotnet4";

        public string SDKManager = "AmaSDKManager";

        public string[] MediationAdapters = new string[] {
            "IronSourceTikTokAdapter",
            "IronSourceAdColonyAdapter",
            "ISAdMobEditor",
            "IronSourceAmazonAdapter",
            "IronSourceAppLovinAdapter",
            "IronSourceMintegralAdapter",
            "IronSourceUnityAdsAdapter",
            "IronSourceFacebookAdapter",
        };

        public bool IsSelectPackage(string package)
        {
            int val = 1;
            m_PackageSelectCache.TryGetValue(package, out val);
            return val > 0;
        }

        public void SelectPackage(string package, bool select)
        {
            m_PackageSelectCache[package] = select ? 1 : 0;
            var p = Packages.FirstOrDefault(i => i.Name == package);
            if (p != null)
            {
                PlayerPrefs.SetInt(p.GetPrefName(), m_PackageSelectCache[package]);
            }
        }

        private string GetDefaultPackageVersion(string package)
        {
            var p = Packages.FirstOrDefault(item => item.Name == package);
            if (p != null)
            {
                return p.Version;
            }
            return null;
        }

        public string GetPackageVersion(string package)
        {
            var version = PlayerPrefs.GetString($"__version__{package}", null);
            if (string.IsNullOrEmpty(version))
            {
                var packages = PackageHelper.FindInstalledPackages(package);
                if (packages.Count > 0)
                {
                    version = packages[0].Version;
                }
                else
                {
                    version = GetDefaultPackageVersion(package);
                }
            }

            return version;
        }

        public void SetPackageVersion(string package, string version)
        {
            PlayerPrefs.SetString($"__version__{package}", version);
            PlayerPrefs.Save();
        }

        
    }
}