using  Amanotes.TripleSDK.SDKSetupWizard;
using UnityEditor;


namespace GooglePlayServices {
    public static class HackGoogleServiceResolver
    {
        public static void FixGoogleServiceResolver()
        {
            var dialog = Utils.GoogleResolverSettingDialog;
            if(dialog == null) {
                if(EditorUtility.DisplayDialog("Error",
                        @"You need to fix import and try again. Do you want to fix importing issues now?", "OK", "Cancel"))
                        {
                            Utils.FixGoogleServiceResolver();
                        }
            }
            else 
            {
                Utils.SetNonPublicStaticProperty(dialog, "UseJetifier", true);
                Utils.SetNonPublicStaticProperty(dialog, "PatchMainTemplateGradle", false);
            }
        }
    }
}