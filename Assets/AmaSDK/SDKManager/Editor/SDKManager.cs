﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;
using System.Text.RegularExpressions;
using System.Reflection;
using GooglePlayServices;
using UnityEditor.Callbacks;
#if UNITY_ANDROID && UNITY_2019
using UnityEditor.Android;
#endif

namespace Amanotes.TripleSDK.SDKSetupWizard
{
    [Serializable]
    public class ServiceConfigurations
    {
        [SerializeField]
        public ServiceConfiguration[] configs;

        public bool IsEnable(string serviceName)
        {
            if (configs != null)
            {
                var config = configs.FirstOrDefault(e => e.name.Equals(serviceName, StringComparison.OrdinalIgnoreCase));
                if (config != null) return config.enable;
            }
            return true;
        }
    }

    [Serializable]
    public class ServiceConfiguration
    {
        public string name;
        public bool enable;
    }

    [InitializeOnLoad]
#if UNITY_2019 && UNITY_ANDROID
    public class SDKManager : EditorWindow, IPostGenerateGradleAndroidProject
#else
    public class SDKManager : EditorWindow
#endif
    {
        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnScriptsReloaded()
        {

            // StartFileWatcher(Application.dataPath);
            // do something
            var val = PlayerPrefs.GetInt("FrameworkSetupWizard", 0);
            if (val == 0)
            {
                ShowWindow();
                PlayerPrefs.SetInt("FrameworkSetupWizard", 1);
                PlayerPrefs.Save();
            }

            PackageHelper.StaticInit();
        }

        public void OnPostGenerateGradleAndroidProject(string path)
        {
            if (IsEnableMultidex)
            {
                var root = Directory.GetParent(Application.dataPath).FullName;
                var launcherTemplateFile = Path.Combine(root, "Temp/gradleOut/launcher/build.gradle");
                if (File.Exists(launcherTemplateFile))
                {
                    EnableGradleMultidex(launcherTemplateFile);
                }
            }

        }

        private bool IsEnableMultidex
        {
            get
            {
                var filename = "mainTemplate.gradle";
                var targetPath = Path.Combine(Application.dataPath, string.Format("Plugins/Android/{0}", filename));
                if (File.Exists(targetPath))
                {
                    var lines = File.ReadAllLines(targetPath);
                    Regex rx = new Regex(@"\s+multiDexEnabled\s+true",
                    RegexOptions.Compiled);
                    foreach (var line in lines)
                    {
                        if (rx.IsMatch(line))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        private int PM_VERSION = 0;

        private void OnEnable()
        {
            // StartFileWatcher(Application.dataPath);
            CheckImportConfigurations();
            PM_VERSION = PlayerPrefs.GetInt("PM_VERSION", 1);
        }

        private static bool IsGoogleServiceJsonExist = false;
        private static bool IsGoogleServiceInfoExist = false;
        private static bool IsIronSourceFileExists = false;
        private static bool IsAppslfyerFileExists = false;

        private void Awake()
        {
            if (PackageHelper.IsPackageInstalled(configuration.FirebasePackage))
            {
                if (Utils.GooglePlayServicesImpl == null)
                {
                    Utils.FixGoogleServiceResolver();
                }
            }

        }

        private void VerifyFilesExists()
        {

            string googleServiceJson = Path.Combine(Application.dataPath, "google-services.json");
            string googleServiceInfo = Path.Combine(Application.dataPath, "GoogleService-Info.plist");

            IsGoogleServiceJsonExist = File.Exists(googleServiceJson);
            IsGoogleServiceInfoExist = File.Exists(googleServiceInfo);
            IsIronSourceFileExists = File.Exists(Path.Combine(Application.dataPath, "ironsource.json"));
            IsAppslfyerFileExists = File.Exists(Path.Combine(Application.dataPath, "appsflyer.json"));

        }

        private Configuration m_config;
        Configuration configuration
        {
            get
            {
                if (m_config == null)
                {
                    m_config = new Configuration();
                }
                return m_config;
            }
        }

        [MenuItem("Amanotes/SDK Manager")]
        private static void ShowWindow()
        {
            var window = GetWindow<SDKManager>();
            window.titleContent = new GUIContent("SDK Manager Window");
            window.minSize = new Vector2(800, 600);
            PackageHelper.StaticInit();
            // window.maxSize = new Vector2(Screen.width - 30, Screen.height - 30);
            window.Show();
        }

        private void RenderPackageSelect(string package, string refLink = null, Action customInstallAction = null)
        {

            GUILayout.Space(20);
            // GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height), package);
            GUILayout.BeginHorizontal();
            // package toggle
            var isInstalled = PackageHelper.IsPackageInstalled(package);
            var isSelect = configuration.IsSelectPackage(package);
            var toggle = GUILayout.Toggle(isSelect, package);
            if (toggle != isSelect)
            {
                configuration.SelectPackage(package, toggle);
            }
            if (!string.IsNullOrEmpty(refLink))
            {
                if (GUILayout.Button("?", GUILayout.Width(30)))
                {
                    Application.OpenURL(refLink);
                }
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();



            if (PackageHelper.PM != null)
            {
                // version select
                var packages = PackageHelper.FindPackages(package);
                var options = (from p in packages select p.Version).ToList();
                var version = configuration.GetPackageVersion(package);
                int selected = options.IndexOf(version);
                var newSelected = EditorGUILayout.Popup("Version", selected, options.ToArray());
                if (newSelected != selected)
                {
                    configuration.SetPackageVersion(package, options[newSelected]);
                }

                // install button 
                if (GUILayout.Button(isInstalled ? "Update" : "Install"))
                {
                    if (!isInstalled || PM_VERSION < 2 || EditorUtility.DisplayDialog("Confirm",
                    @"You should remove package before update before update. Do you want to continue updating?", "Yes", "Cancel"))
                    {
                        if (customInstallAction != null)
                        {
                            customInstallAction.Invoke();
                        }
                        else
                        {
                            PackageHelper.AddPackage(package, configuration.GetPackageVersion(package));
                            PackageHelper.SavePackages();

                        }
                    }

                }
                if (isInstalled)
                {
                    var isEnabled = ServiceConfigs.IsEnable(package);
                    if (GUILayout.Button(isEnabled ? "Disable" : "Enable"))
                    {
                        SetEnableService(package, !isEnabled);
                    }
                    if (PM_VERSION >= 2 && GUILayout.Button("Remove"))
                    {

                        PackageHelper.RemovePackage(package);
                    }
                }
            }

            GUILayout.EndHorizontal();


        }

        void GUILine(int i_height = 1)
        {
            Rect rect = EditorGUILayout.GetControlRect(false, i_height);
            rect.height = i_height;
            EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
        }

        private void RenderPackageSession(string title,
            string refLink = null, Action customRender = null)
        {

            GUILayout.BeginHorizontal();
            GUILayout.Label($"* {title}", EditorStyles.boldLabel);

            if (customRender != null)
            {
                customRender.Invoke();
            }


            if (!string.IsNullOrEmpty(refLink))
            {
                if (GUILayout.Button("?", GUILayout.Width(30)))
                {
                    Application.OpenURL(refLink);
                }
            }
            GUILayout.EndHorizontal();
        }

        Vector2 scrollPosition = new Vector2(15, 15);
        // TextField m_TextFieldBinding;
        private void OnGUI()
        {
            VerifyFilesExists();

            // GUILayout.BeginArea(new Rect(15, 15, Screen.width - 30, Screen.height - 30));
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            RenderPackageManagerSession();
            GUILine();

            RenderAppsflyerSetup();
            GUILine();

            RenderSetupFirebase();
            GUILine();

            RenderAmaUnityIAP();
            GUILine();

            RenderIronSource();
            GUILine();

            RenderISMediationAdapters();
            GUILine();

            RenderInstallAllPackages();

            GUILine();
            RenderMultiDex();


            GUILayout.Space(5);
            GUILayout.EndScrollView();

        }

        void RenderPackageManagerSession()
        {
            RenderPackageSession("Package Manager (required Unity 2018.3.0b7 or above.)", configuration.refPackageManager,
                PackageManagerCustomAction);

            GUILayout.BeginHorizontal();
            var instance = PackageHelper.PM;
            if (instance != null)
            {
                if (GUILayout.Button("Update SDK Manager"))
                {
                    Action<string> action = CheckUpdate;
                    Utils.InvokeStaticMethod(instance, "UpdatePackageList",
                        new object[] {
                            action
                    });

                }
            }

            if (GUILayout.Button(instance == null ? "Install Package Manager" : "Update Package Manager"))
            {
                if (instance != null)
                {
                    Action<string> action = OnFinishUpdatePackageList;
                    Utils.InvokeStaticMethod(instance, "UpdatePackageList",
                        new object[] {
                            action
                    });

                }
                else
                {
                    AddPackageManager();
                }
            }
            if (instance != null)
            {

                if (GUILayout.Button("Delete Cache Files"))
                {
                    var files = Directory.GetFiles(Application.persistentDataPath);
                    foreach (var file in files)
                    {
                        try
                        {
                            Debug.LogWarning("Delete: " + file);
                            File.Delete(file);
                        }
                        catch (Exception ex)
                        {

                            //Debug.LogError(ex.Message);
                        }

                    }

                    EditorUtility.ClearProgressBar();
                }
            }
            GUILayout.EndHorizontal();
        }

        void PackageManagerCustomAction()
        {

        }

        void OnFinishUpdatePackageList(string info)
        {

            AddPackageManager();
        }

        private void RenderAppsflyerSetup()
        {
            var installed = PackageHelper.IsPackageInstalled(configuration.AppsflyerPackage);

            RenderPackageSelect(configuration.AppsflyerPackage,
                configuration.refAppsflyer);

            if (installed)
            {
                DisplayValidateToggle(IsAppslfyerFileExists, "Is appsflyer.json file imported to Assets folder?");
            }
        }


        private void InstallFirebase()
        {
            PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.Android, ApiCompatibilityLevel.NET_4_6);
            PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.iOS, ApiCompatibilityLevel.NET_4_6);
            var package = this.configuration.FirebasePackage;
            PackageHelper.AddPackage(package, configuration.GetPackageVersion(package));
            PackageHelper.SavePackages();

        }

        private void RenderSetupFirebase()
        {
            RenderPackageSelect(configuration.FirebasePackage, configuration.refFirebase,
               InstallFirebase);

            var installed = PackageHelper.IsPackageInstalled(configuration.FirebasePackage);
            if (installed)
            {
                DisplayValidateToggle(IsGoogleServiceJsonExist,
                               "Is google-services.json imported? (Copy google-services.json file to Assets folder)");
                DisplayValidateToggle(IsGoogleServiceInfoExist,
                    "Is google-services-info.plist imported? (Copy google-services-info.plist file to Assets folder)");

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("Fix Plugin Importing Issues"))
                {
                    Utils.FixGoogleServiceResolver();
                }
                if (GUILayout.Button("Update Google Services Version"))
                {
                    Utils.InvokeVersionHandlerMethod("UpdateNow", null, null);
                }

                if (GUILayout.Button("Force Resolve Google Services"))
                {
                    if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
                    {
                        if (EditorUtility.DisplayDialog("Switch Platform",
                        @"You need switch to Android platfrom before resolving Google Services then resolve Google Services again.
Do you want to switch to Android platfrom?", "OK", "Cancel"))
                        {
                            if (EditorUserBuildSettings.SwitchActiveBuildTarget(
                                BuildTargetGroup.Android,
                                BuildTarget.Android))
                            {

                            };
                        }
                    }
                    else
                    {
                        Utils.InvokeGooglePlayServicesMethod("MenuForceResolve", null, null);
                    }
                }
                if (GUILayout.Button("Generate Resource"))
                {
                    ForceGenerateFirebaseResource();
                }
                GUILayout.EndHorizontal();
            }

        }

        private void RenderIronSource()
        {
            RenderPackageSelect(configuration.IronSourcePackage, configuration.refIronSource);

            var installed = PackageHelper.IsPackageInstalled(configuration.IronSourcePackage);
            if (installed)
            {
                DisplayValidateToggle(IsIronSourceFileExists, "Is ironsource.json file imported to Assets folder?");
            }

        }

        private void RenderISMediationAdapters()
        {
            GUILayout.Label("Add IronSource Mediation Adapters:", EditorStyles.boldLabel);
            RenderPackageList(configuration.MediationAdapters, "Add Adapters");
        }

        private void RenderMultiDex()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label($"* Enable Android Multidex", EditorStyles.boldLabel);
#if !UNITY_ANDROID
            if (GUILayout.Button("Switch to Android Platform", GUILayout.Width(180)))
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(
                                            BuildTargetGroup.Android,
                                            BuildTarget.Android);
            }
#endif
            if (GUILayout.Button("Enable", GUILayout.Width(100)))
            {
                if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
                {
                    if (EditorUtility.DisplayDialog("Switch Platform",
                    @"You must switch to Android platfrom.", "OK", "Cancel"))
                    {
                        if (EditorUserBuildSettings.SwitchActiveBuildTarget(
                            BuildTargetGroup.Android,
                            BuildTarget.Android))
                        {
                            AddMultidexSupport();
                        };
                    }
                }
                else
                {
                    AddMultidexSupport();
                }

            }

            GUILayout.EndHorizontal();
        }

        private void CustomIAPRender()
        {

            GUILayout.Label("You need to setup Unity IAP to install this package.", EditorStyles.boldLabel);
            if (GUILayout.Button("Setting up Unity IAP guide"))
            {
                Application.OpenURL("https://docs.unity3d.com/Manual/UnityIAPSettingUp.html");
            }
        }

        private void RenderAmaUnityIAP()
        {
            if (PackageHelper.UnityIAPConfigurationBuilder != null)
            {
                RenderPackageSelect(configuration.AmaUnityIAP, configuration.refIAP);
                //    GUILayout.Label("Package Manager ", EditorStyles.boldLabel);
            }
            else
            {
                Action action = CustomIAPRender;
                RenderPackageSession(configuration.AmaUnityIAP, configuration.refIAP, action);

            }
        }

        private void RenderInstallAllPackages()
        {
            GUILayout.Space(20);
            if (PackageHelper.PM != null)
            {
                if (GUILayout.Button("Install selected packages"))
                {
                    if (PackageHelper.UnityIAPConfigurationBuilder == null)
                    {
                        configuration.SelectPackage(configuration.AmaUnityIAP, false);
                    }

                    var packages = PackageHelper.Packages;
                    var toInstalls = new List<string>();
                    foreach (var pkg in packages)
                    {
                        if (configuration.IsSelectPackage(pkg.Name))
                        {
                            if (pkg.Name == configuration.FirebasePackage)
                            {
                                PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.Android, ApiCompatibilityLevel.NET_4_6);
                                PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.iOS, ApiCompatibilityLevel.NET_4_6);
                                var package = this.configuration.FirebasePackage;
                                PackageHelper.AddPackage(package, configuration.GetPackageVersion(package));
                            }
                            else
                            {
                                PackageHelper.AddPackage(pkg.Name,
                                                               configuration.GetPackageVersion(pkg.Name));
                            }

                        }
                    }
                    PackageHelper.SavePackages();

                }
            }
        }

        private void RenderPackageList(IEnumerable<string> pkgs,
            string installButton = null)
        {
            var pm = PackageHelper.PM;



            foreach (var pkg in pkgs)
            {
                GUILayout.BeginHorizontal();
                var isSelect = configuration.IsSelectPackage(pkg);
                var isInstalled = PackageHelper.IsPackageInstalled(pkg);
                var packages = PackageHelper.FindPackages(pkg);

                //float originalValue = EditorGUIUtility.labelWidth;
                //EditorGUIUtility.labelWidth = 400;
           
                EditorGUI.BeginDisabledGroup(!configuration.Interactive(pkg));
                var toggle = GUILayout.Toggle(isSelect, string.Format("{0,-50}", pkg), GUILayout.Width(300));
                EditorGUI.EndDisabledGroup();
                //EditorGUIUtility.labelWidth = originalValue;
                if (toggle != isSelect)
                {
                    configuration.SelectPackage(pkg, toggle);
                }
               
                // version select 
                if (pm != null)
                {
               
                    var options = (from p in packages select p.Version).ToList();
                    var version = configuration.GetPackageVersion(pkg);
                    int selected = options.IndexOf(version);
                    var newSelected = EditorGUILayout.Popup("Version", selected, options.ToArray());

                    if (newSelected != selected)
                    {
                        configuration.SetPackageVersion(pkg, options[newSelected]);
                    }

                    // install button 
                    if (GUILayout.Button(isInstalled ? "Update" : "Install"))
                    {
                        if (!isInstalled || PM_VERSION < 2 || EditorUtility.DisplayDialog("Confirm",
                            @"You should remove package before update before update. Do you want to continue updating?", "Yes", "Cancel"))
                        {
                            PackageHelper.AddPackage(pkg, configuration.GetPackageVersion(pkg));
                            PackageHelper.SavePackages();
                        }

                    }
                    if (isInstalled)
                    {
                        if (PM_VERSION >= 2 && GUILayout.Button("Remove"))
                        {
                            PackageHelper.RemovePackage(pkg);
                        }
                    }
                }


                GUILayout.EndHorizontal();
            }

            // if (!string.IsNullOrEmpty(installButton))
            // {
            //     if (GUILayout.Button(installButton))
            //     {
            //         foreach (var package in packages)
            //         {
            //             var isSelect = configuration.IsSelectPackage(package);
            //             if (isSelect)
            //             {
            //                 PackageHelper.AddPackage(package, configuration.GetPackageVersion(package));
            //             }
            //         }
            //         PackageHelper.SavePackages();
            //     }
            // }
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            // VerifyFilesExists();
        }

        private void EndColor()
        {
            GUI.color = Color.black;
        }

        private void DisplayValidateToggle(bool valid, string text)
        {
            Color before = GUI.color;
            GUI.color = valid ? Color.green : Color.red;
            GUILayout.Toggle(valid,
                text);
            GUI.color = before;
        }


        static AddRequest Request;
        private static void AddPackageManager()
        {
            Request = Client.Add("com.amanotes.packagemanager@https://lttrieu@bitbucket.org/lttrieu/test-amanotes-package-manager.git");
            EditorApplication.update += Progress;
        }

        static void Progress()
        {
            if (Request.IsCompleted)
            {
                if (Request.Status == StatusCode.Success)
                    Debug.Log("Installed: " + Request.Result.packageId);
                else if (Request.Status >= StatusCode.Failure)
                    Debug.Log(Request.Error.message);

                EditorApplication.update -= Progress;
                EditorUtility.ClearProgressBar();
                
            }
            else
            {
                EditorUtility.DisplayProgressBar("Install Package Manager",
                       "Installing...", 0.5f);
            }
        }


        private static ServiceConfigurations serviceConfigs;

        public static ServiceConfigurations ServiceConfigs
        {
            get
            {
                if (serviceConfigs == null)
                {
                    var textAsset = Resources.Load<TextAsset>("ServiceConfigurations");
                    if (textAsset == null)
                    {
                        //                         File.WriteAllText(Path.Combine(Application.dataPath, "/Resources/ServiceConfigurations.json"),
                        // @"{
                        //     ""configs"": []
                        // }");

                        serviceConfigs = new ServiceConfigurations() { configs = new ServiceConfiguration[0] };

                    }
                    else
                    {
                        serviceConfigs = JsonUtility.FromJson<ServiceConfigurations>(textAsset.text);
                        if (serviceConfigs.configs == null) serviceConfigs.configs = new ServiceConfiguration[0];
                    }


                }
                return serviceConfigs;
            }
        }

        public static void SetEnableService(string name, bool enable)
        {
            if (ServiceConfigs.configs == null) ServiceConfigs.configs = new ServiceConfiguration[0];

            var config = ServiceConfigs.configs.FirstOrDefault(e => e.name == name);
            if (config == null)
            {
                var configs = new List<ServiceConfiguration>(ServiceConfigs.configs);
                configs.Add(new ServiceConfiguration()
                {
                    name = name,
                    enable = enable,
                });
                ServiceConfigs.configs = configs.ToArray();
            }
            else
            {
                config.enable = enable;
            }

            var json = JsonUtility.ToJson(serviceConfigs, true);
            var dir = Path.Combine(Application.dataPath, "Resources");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            var path = Path.Combine(Application.dataPath, "Resources/ServiceConfigurations.json");
            File.WriteAllText(path, json);
        }


        public static string ANDROID_MANIFEST
        {
            get
            {
                return Path.Combine(Application.dataPath, "Plugins/Android/AndroidManifest.xml");
            }
        }

        public int callbackOrder => 0;

        private static void EnableGradleMultidex(string targetPath)
        {


            var lines = File.ReadAllLines(targetPath);
            // checking multiDexEnabled
            int cnt = 0;
            int defaultBlockLine = 0;

            Regex rx = new Regex(@"\s+multiDexEnabled\s+true",
                RegexOptions.Compiled);
            var found = false;
            while (cnt < lines.Length)
            {
                var line = lines[cnt];
                if (line.IndexOf("defaultConfig") >= 0)
                {
                    defaultBlockLine = cnt;
                }
                var matches = rx.Match(line);
                if (matches.Length > 0)
                {
                    found = true;
                    break;
                }
                cnt++;
            }
            if (!found)
            {
                var _lines = new List<string>(lines);
                _lines.Insert(defaultBlockLine + 2, "\t\tmultiDexEnabled true");
                lines = _lines.ToArray();
            }

            File.WriteAllLines(targetPath, lines);
            // checking deps
        }

        private static void AddMultidexSupport()
        {
            var filename = "mainTemplate.gradle";
            // add android manifest
            // add support 
            // PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevel28;
            var p = Path.GetDirectoryName(EditorApplication.applicationPath);

            // Step 1: Custom Grade

#if UNITY_EDITOR_WIN
            var templatePath = Path.Combine(p, string.Format("Data\\PlaybackEngines\\AndroidPlayer\\Tools\\GradleTemplates\\{0}", filename));
            //var mainTemplatePath = Path.Combine(Application.dataPath, "AmaSDK\\SDKManager\\Editor\\gradle.template");
            var targetPath = Path.Combine(Application.dataPath, string.Format("Plugins\\Android\\{0}", filename));


#elif UNITY_EDITOR_OSX || UNITY_EDITOR_LINUX
            var templatePath = Path.Combine(p, string.Format("PlaybackEngines/AndroidPlayer/Tools/GradleTemplates/{0}", filename));
            var targetPath = Path.Combine(Application.dataPath, string.Format("Plugins/Android/{0}", filename));
#endif

            if (File.Exists(templatePath))
            {
                if (File.Exists(targetPath))
                {
                    File.Delete(targetPath);
                }

                File.Copy(templatePath, targetPath);

                EnableGradleMultidex(targetPath);

            }



            var path = ANDROID_MANIFEST;
            var doc = Utils.InvokeStaticMethod(Utils.AndroidManifestHelper, "LoadXmlDocument", new object[] { path });
            // Step 2: Add 
            if (doc != null)
            {
                Utils.InvokeStaticMethod(Utils.AndroidManifestHelper, "AddApplicationAttribute", new object[] {
                    doc, "name", "android.support.multidex.MultiDexApplication"
                });
                // AndroidManifestHelper.AddApplicationAttribute(doc, "name", "android.support.multidex.MultiDexApplication");
                // AndroidManifestHelper.SaveXml(doc, path);
                Utils.InvokeStaticMethod(Utils.AndroidManifestHelper, "SaveXml", new object[] {
                     doc, path
                 });

                // UnityEngine.Debug.Log($"Amazon prebuild processing is done.");
            }
            else
            {
                Debug.LogWarning($"{path} not found.");
            }

            HackGoogleServiceResolver.FixGoogleServiceResolver();

            Utils.InvokeGooglePlayServicesMethod("MenuForceResolve", null, null);
        }

        public static void ForceGenerateFirebaseResource()
        {
            if (PackageHelper.GenerateXmlFromGoogleServicesJson != null)
            {
                Utils.InvokeStaticMethod(PackageHelper.GenerateXmlFromGoogleServicesJson, "ForceJsonUpdate", new object[] {
                    true
                });
            }
        }

        public void CheckUpdate(string packageListFile)
        {
            PackageHelper.StaticInit();

            var packages = PackageHelper.AllPackages.Packages
                .Where(e => e.Name == this.configuration.SDKManager);

            var latestPkg = packages.OrderByDescending(e => e.GetVersionCode())
                .FirstOrDefault();

            var installedPkg = PackageHelper.FindInstalledPackages(this.configuration.SDKManager)
                .FirstOrDefault();

            if (latestPkg != null && (installedPkg == null ||
                installedPkg.GetVersionCode() < latestPkg.GetVersionCode()))
            {
                PackageHelper.AddPackage(configuration.SDKManager, latestPkg.Version);
                PackageHelper.SavePackages();
            }
        }

        [PostProcessBuildAttribute(1)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string pathToBuiltProject)
        {
            if (buildTarget == BuildTarget.Android)
            {

            }
        }

        public void CheckImportConfigurations()
        {
            var searchFolders = new string[] { "Assets/Resources" };
            bool needImportAsset = false;

            VerifyFilesExists();
            if (IsIronSourceFileExists)
            {
                var assets = AssetDatabase.FindAssets("ironsource", searchFolders);
                if (assets != null)
                {
                    var name = (from a in assets select Path.GetFileName(AssetDatabase.GUIDToAssetPath(a)))
                        .FirstOrDefault(e => e == "ironsource.asset");
                    if (string.IsNullOrEmpty(name))
                    {
                        needImportAsset = true;
                    }
                }
            }

            if (IsAppslfyerFileExists)
            {
                var assets = AssetDatabase.FindAssets("appsflyer", searchFolders);
                if (assets != null)
                {
                    var name = (from a in assets select Path.GetFileName(AssetDatabase.GUIDToAssetPath(a)))
                        .FirstOrDefault(e => e == "appsflyer.asset");
                    if (string.IsNullOrEmpty(name))
                    {
                        needImportAsset = true;
                    }
                }
            }

            if (needImportAsset)
            {
                var type = Utils.FindClass(null, "Amanotes.TripleSDK.Core.Configuration.ConfigurationEditor");
                if (type != null)
                {
                    Utils.InvokeStaticMethod(type, "Import", new object[] { });
                }
            }
        }

    }

}