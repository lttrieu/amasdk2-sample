using UnityEngine;

[CreateAssetMenu(fileName = "GoogleAdConfig", menuName = "Amanotes/IronSource/Create GoogleAdConfig", order = 0)]
public class GoogleAdConfig : ScriptableObject {
    
    [Tooltip("Android: AdMob’s SDK requires publishers to add their AdMob App ID to the app’s AndroidManifest as a child of the <application></application> tag")]
    public string AppID;

    [Tooltip("iOS: AdMob’s SDK requires publishers to add their AdMob App ID to the app’s plist ")]
    public string GADApplicationIdentifier;

}