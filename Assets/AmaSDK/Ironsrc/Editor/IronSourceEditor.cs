using UnityEngine;
using UnityEditor;
using System.IO;

namespace Amanotes.TripleSDK.Ironsrc
{
    public class IronSourceEditor
    {
        public static string ANDROID_MANIFEST {
            get {
                return Path.Combine(Application.dataPath, "Plugins/Android/IronSource/AndroidManifest.xml");
            }
        }

        public static string ROOT_ANDROID_DIR
        {
            get
            {
                return Path.Combine(Application.dataPath, "Plugins/Android/IronSource/libs");
            }
        }
    }
}