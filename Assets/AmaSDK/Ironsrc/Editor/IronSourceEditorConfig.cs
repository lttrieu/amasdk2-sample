using UnityEngine;

namespace Amanotes.TripleSDK.Ironsrc
{
    [CreateAssetMenu(fileName = "IronSourceEditorConfig", menuName = "Amanotes/IronSource/IronSourceEditorConfig", order = 0)]
    public class IronSourceEditorConfig : ScriptableObject {
        public bool ITSAppUsesNonExemptEncryption = false;
        public bool AndroidMultidex = true;
        public string IronSourceVersion;
    }
}
