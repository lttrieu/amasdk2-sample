﻿
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEngine;

using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using System.Xml;
using Amanotes.TripleSDK.Core;
using System;

namespace Amanotes.TripleSDK.Ironsrc
{
    [InitializeOnLoad]
    public class IronSourceBuildProcessor : IPreprocessBuildWithReport
    {
        static IronSourceBuildProcessor()
        {
            CreateDefaultConfig(); 
        }
         
        private static void CreateDefaultConfig()
        {
            var file = Path.Combine(Application.dataPath, "ironsource.json");
            if (!File.Exists(file))
            {
                var content = @"
{
    ""appKeyIos"": ""acc9e3f5"",
    ""appKeyAndroid"": ""b7eacf9d""
}
            ";
                File.WriteAllText(file, content);
            }
        }

        public int callbackOrder { get { return 0; } }

        public void OnPreprocessBuild(BuildReport report)
        {
            if(report.summary.platform == BuildTarget.Android) {
                UpdateAndroidManifest();
                RemoveOldLibrary();
            }
        }

        private void RemoveOldLibrary()
        {
          
            var config = LoadConfig();
            if (config == null) return;

            var files = Directory.GetFiles(IronSourceEditor.ROOT_ANDROID_DIR, "mediationsdk-*.jar");
            foreach (var file in files)
            {
                var fileName = Path.GetFileNameWithoutExtension(file);
                if (!fileName.Contains(config.IronSourceVersion)) {
                    try
                    {
                        File.Delete(file);
                        Debug.LogWarning("Delete duplicated lib: " + file);
                    }
                    catch (IOException)
                    {
                        Debug.LogWarning("Could not duplicated lib: " + file);
                    }
                }
               
            }
        }

        [PostProcessBuildAttribute(1)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string pathToBuiltProject)
        {

            if (buildTarget == BuildTarget.iOS)
            {
                var config = LoadConfig();
                var adMobConfig = LoadAdMobConfig();

#if UNITY_IOS
                ////// GADApplicationIdentifier
                // Get plist
                string plistPath = Path.Combine(pathToBuiltProject, "Info.plist");
                PlistDocument plist = new PlistDocument();
                plist.ReadFromString(File.ReadAllText(plistPath));

                // Get root
                PlistElementDict rootDict = plist.root;

                if(adMobConfig != null)
                {
                    // admob
                    rootDict.SetString("GADApplicationIdentifier", adMobConfig.GADApplicationIdentifier);
                }

                //// OTHERS
                // Get plist
            
                if(config != null)
                {
                    // Missing Compliance in Status when i add built for internal testing in Test Flight.How to solve?
                    rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", config.ITSAppUsesNonExemptEncryption);
                }
                else 
                {
                    rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);
                }

                try
                {
                    PlistElementDict NSAppTransportSecurity = rootDict.CreateDict("NSAppTransportSecurity");
                    NSAppTransportSecurity.SetBoolean("NSAllowsArbitraryLoads", true);
                }
                catch (System.Exception)
                {
                  
                }
              
                // Write to file
                File.WriteAllText(plistPath, plist.WriteToString());
#endif
            }
        }

        [MenuItem("Amanotes/IronSource/Update AndroidManifest")]
        private static void UpdateAndroidManifest()
        {

            XmlDocument root = AndroidManifestHelper.LoadXmlDocument(IronSourceEditor.ANDROID_MANIFEST);
            if (root != null)
            {
                var config = LoadAdMobConfig();
                if (config != null)
                {
                    AndroidManifestHelper.AddApplicationMetaData(root,
                        "com.google.android.gms.ads.APPLICATION_ID", config.AppID);
                }
                else
                {
                    Debug.LogError("AdMob AppID not found!");
                }


                //AndroidManifestHelper.AddActivity(
                //    root,
                //    "com.google.android.gms.ads.AdActivity",
                //    new System.Collections.Generic.Dictionary<string, string>(){
                //        {"configChanges", "keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"},
                //        {"theme", "@android:style/Theme.Translucent"}
                //    });
                AndroidManifestHelper.SaveXml(root, IronSourceEditor.ANDROID_MANIFEST);

                UnityEngine.Debug.Log($"AdMob prebuild processing is done.");
            }

        }

        private static GoogleAdConfig LoadAdMobConfig()
        {
            var configFile = "Assets/AmaSDK/Ironsrc/Editor/GoogleAdConfig.asset";

            var config = AssetDatabase.LoadAssetAtPath<GoogleAdConfig>(configFile);

            return config;
        }


        private static IronSourceEditorConfig LoadConfig()
        {
            var configFile = "Assets/AmaSDK/Ironsrc/Editor/IronSourceEditorConfig.asset";

            var config = AssetDatabase.LoadAssetAtPath<IronSourceEditorConfig>(configFile);

            return config;
        }

    }

}
