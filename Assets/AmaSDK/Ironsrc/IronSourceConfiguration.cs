using UnityEngine;

namespace Amanotes.TripleSDK.Ironsrc
{
    public class IronSourceConfiguration: ScriptableObject {
        public string appKeyIos;
        public string appKeyAndroid;
        public bool consent;

        public string GetKey()
        {
            #if UNITY_ANDROID
                return appKeyAndroid;
            #elif UNITY_IOS
                return appKeyIos;
            #else
                return "";
            #endif
        }
    }
}