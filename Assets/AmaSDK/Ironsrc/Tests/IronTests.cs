﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Tests
{
    public class IronTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void GradleIntegration()
        {
            // Use the Assert class to test conditions
            var file = Path.Combine(Application.dataPath, "Plugins/Android/mainTemplate.gradle");
            var isExist = File.Exists(file);
            Assert.IsTrue(isExist, "Make source you integrate Gradle https://developers.ironsrc.com/ironsource-mobile/unity/unity-plugin/#step-2");

            if (isExist)
            {
                var lines = File.ReadAllLines(file);

                Regex r1 = new Regex(
                    @"implementation\s+'com.google.android.gms:play-services-ads-identifier:16.0.0'",
                    RegexOptions.Compiled);

                Regex r2 = new Regex(
                    @"implementation\s+'com.android.support:support-v4:27.1.1'",
                    RegexOptions.Compiled);

                Assert.IsTrue(lines.Any(line => r1.IsMatch(line)), "Missing com.google.android.gms:play-services-ads-identifier:16.0.0 in the dependencies block");
                Assert.IsTrue(lines.Any(line => r2.IsMatch(line)), "Missing com.android.support:support-v4:27.1.1 in the dependencies block");
            }
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator IronTestsWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
