﻿using UnityEngine;
using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Core.Configuration;

namespace Amanotes.TripleSDK.Ironsrc
{
    [InjectService(Package = "AmaIronSource")]
    [StartupService]
    [ServiceConfiguration(Name = "ironsource", Type = typeof(IronSourceConfiguration))]
    public class IronSourceService : MonoBehaviour, IStartup
    {
        public IronSourceConfiguration configuration;

        void Start()
        {
       
        }

        public void Startup()
        {
            configuration = Bootstrap.Instance.GetRequiredService<ConfigurationService>()
                .GetConfiguration<IronSourceService, IronSourceConfiguration>();

            IronSource.Agent.setConsent(true);

            if(Debug.isDebugBuild) IronSource.Agent.setAdaptersDebug(true);
            
            // TODO: define your api key
            IronSource.Agent.init(configuration.GetKey(),
                 IronSourceAdUnits.REWARDED_VIDEO,
                 IronSourceAdUnits.INTERSTITIAL,
                 IronSourceAdUnits.OFFERWALL,
                 IronSourceAdUnits.BANNER);

            if(Debug.isDebugBuild) IronSource.Agent.validateIntegration();
        }

        void OnApplicationPause(bool isPaused)
        {
            IronSource.Agent.onApplicationPause(isPaused);
        }

    }
}

