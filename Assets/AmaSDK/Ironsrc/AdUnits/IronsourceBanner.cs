using UnityEngine;
using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Core.Analytics;
using Amanotes.TripleSDK.Core.AdUnits;

namespace Amanotes.TripleSDK.Ironsrc
{
    [InjectService]
    public class IronsourceBanner : MonoBehaviour, IBannerAds
    {
        public string EVENT_BANNER_ADS_SHOW = "bannerads_show";
        public string EVENT_BANNER_CLICK = "bannerads_click";
       
        void Start()
        {
            IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
            IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
            IronSourceEvents.onBannerAdClickedEvent += BannerAdClickedEvent;
            IronSourceEvents.onBannerAdScreenPresentedEvent += BannerAdScreenPresentedEvent;
            IronSourceEvents.onBannerAdScreenDismissedEvent += BannerAdScreenDismissedEvent;
            IronSourceEvents.onBannerAdLeftApplicationEvent += BannerAdLeftApplicationEvent;
        }

        public void HideBanner()
        {
            IronSource.Agent.hideBanner();
        }

        public void LoadBanner(BannerPosition position = BannerPosition.BOTTOM)
        {
            Development.Log($"LoadBanner {position}");
            
            Bootstrap.GetService<IAnalytics>()
                .LogEvent(EVENT_BANNER_CLICK);

            IronSourceBannerPosition p = (IronSourceBannerPosition)position;
            IronSource.Agent.loadBanner(IronSourceBannerSize.SMART,
                p);
        }

        public void ShowBanner()
        {
            IronSource.Agent.displayBanner();
        }

        //Invoked once the banner has loaded
        void BannerAdLoadedEvent()
        {
             Development.Log($"BannerAdLoadedEvent");
        }
        //Invoked when the banner loading process has failed.
        //@param description - string - contains information about the failure.
        void BannerAdLoadFailedEvent(IronSourceError error)
        {
             Development.Log($"BannerAdLoadFailedEvent {error.getDescription()}");
        }

        // Invoked when end user clicks on the banner ad
        void BannerAdClickedEvent()
        {
            Development.Log("BannerAdClickedEvent");

            Bootstrap.GetService<IAnalytics>()
                .LogEvent(EVENT_BANNER_CLICK);

        }
        //Notifies the presentation of a full screen content following user click
        void BannerAdScreenPresentedEvent()
        {
            Development.Log("BannerAdScreenPresentedEvent");

            Bootstrap.GetService<IAnalytics>()
                .LogEvent(EVENT_BANNER_CLICK);
        }
        //Notifies the presented screen has been dismissed
        void BannerAdScreenDismissedEvent()
        {
            Development.Log("BannerAdScreenDismissedEvent");
        }
        //Invoked when the user leaves the app
        void BannerAdLeftApplicationEvent()
        {
            Development.Log("BannerAdLeftApplicationEvent");
        }

    }
}