﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.TripleSDK.Core.AdUnits;
using Amanotes.TripleSDK.Core.Analytics;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Ironsrc
{
    [InjectService()]
    public class IronSourceInterstitial : MonoBehaviour, IInterstitialAds
    {
        //Invoked when the initialization process has failed.
        //@param description - string - contains information about the failure.
        public Action<string> onInterstitialAdLoadFailedEvent { get; set; }

        //Invoked right before the Interstitial screen is about to open.
        public Action onInterstitialAdShowSucceededEvent { get; set; }
        //Invoked when the ad fails to show.
        //@param description - string - contains information about the failure.
        public Action<string> onInterstitialAdShowFailedEvent { get; set; }

        // Invoked when end user clicked on the interstitial ad
        Action onInterstitialAdClickedEvent { get; set; }

        //Invoked when the interstitial ad closed and the user goes back to the application screen.
        public Action onInterstitialAdClosedEvent { get; set; }

        //Invoked when the Interstitial is Ready to shown after load function is called
        public Action onInterstitialAdReadyEvent { get; set; }

        //Invoked when the Interstitial Ad Unit has opened
        Action onInterstitialAdOpenedEvent { get; set; }


        private AdsContext m_context = new AdsContext();

        public AdsContext Context => m_context;

        public bool IsInterstitialReady
        {
            get {
                return IronSource.Agent.isInterstitialReady();
            }
        }

        // Step 1. Implement the Interstitial Events
        void OnEnable()
        {
            IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
            IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
            IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
        }

        void OnDisable()
        {
            IronSourceEvents.onInterstitialAdReadyEvent -= InterstitialAdReadyEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent -= InterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent -= InterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent -= InterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdClickedEvent -= InterstitialAdClickedEvent;
            IronSourceEvents.onInterstitialAdOpenedEvent -= InterstitialAdOpenedEvent;
            IronSourceEvents.onInterstitialAdClosedEvent -= InterstitialAdClosedEvent;
        }

        // // Step 2. Load Interstitial Ad
        // void Start()
        // {
        //     IronSource.Agent.loadInterstitial();
        // }

        //Step 3. Check Ad Availability
        // IronSource.Agent.isInterstitialReady()
        //Invoked when the Interstitial is Ready to shown after load function is called
        void InterstitialAdReadyEvent()
        {
            SafeInvolke(onInterstitialAdReadyEvent);
            LogFullAds(FullAdsTypes.EVENT_FULLADS_REQUEST_SUCCESS);
        }

        public void LogFullAds(string eventName)
        {
            var service = Bootstrap.GetService<IAnalytics>();
            if (service == null)
            {
                Debug.LogWarning("Missing an analytics provider");
            }
            else
            {
                service.LogEvent(eventName, new Dictionary<string, object>  {
                    {AdsContext.SONG_ID, Context.SongId},
                    {AdsContext.SONG_NAME, Context.SongName},
                    {AdsContext.SCREEN, Context.Screen},
                    {AdsContext.LEVEL, Context.Level},
                });
            }
        }


        protected void SafeInvolke(Action action)
        {
            if (action != null)
            {
                try
                {
                    action.Invoke();
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Ads Analytics error: " + ex.Message);
                }
            }
        }
         protected void SafeInvolke(Action<string> action, string param)
        {
            if (action != null)
            {
                try
                {
                    action.Invoke(param);
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Ads Analytics error: " + ex.Message);
                }
            }
        }
       
        //Invoked when the initialization process has failed.
        //@param description - string - contains information about the failure.
        void InterstitialAdLoadFailedEvent(IronSourceError error)
        {
            LogFullAds(FullAdsTypes.EVENT_FULLADS_REQUEST_FAILED);
            SafeInvolke(onInterstitialAdLoadFailedEvent, error.getDescription());
        }

        //Invoked right before the Interstitial screen is about to open.
        void InterstitialAdShowSucceededEvent()
        {
            SafeInvolke(onInterstitialAdShowSucceededEvent);
            LogFullAds(FullAdsTypes.EVENT_FULLADS_SHOW);
        }

        //Invoked when the ad fails to show.
        //@param description - string - contains information about the failure.
        void InterstitialAdShowFailedEvent(IronSourceError error)
        {
            if (error.getCode() == 509)
            {
                LogFullAds(FullAdsTypes.EVENT_FULLADS_SHOW_NOTREADY);
            }
            else
            {
                LogFullAds(FullAdsTypes.EVENT_FULLADS_SHOW_FAILED);
            }
            SafeInvolke(onInterstitialAdShowFailedEvent, error.getDescription());
        }

        // Invoked when end user clicked on the interstitial ad
        void InterstitialAdClickedEvent()
        {
            LogFullAds(FullAdsTypes.EVENT_FULLADS_CLICK);
        }
        //Invoked when the interstitial ad closed and the user goes back to the application screen.
        void InterstitialAdClosedEvent()
        {
            LogFullAds(FullAdsTypes.EVENT_FULLADS_FINISH);
            SafeInvolke(onInterstitialAdClosedEvent);
        }

        //Invoked when the Interstitial Ad Unit has opened
        void InterstitialAdOpenedEvent()
        {
            //SafeInvolke(onInterstitialAdOpenedEvent)
        }

        public void LoadInterstitial()
        {
            LogFullAds(FullAdsTypes.EVENT_FULLADS_REQUEST);
            IronSource.Agent.loadInterstitial();
        }

        public void ShowInterstitial()
        {
           IronSource.Agent.showInterstitial();
        }

        public void ShowInterstitial(string placementName)
        {
            IronSource.Agent.showInterstitial(placementName);
        }
    }
}