﻿using System.Collections.Generic;
using UnityEngine;
using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Core.Analytics;
using Amanotes.TripleSDK.Core.AdUnits;
using System;

namespace Amanotes.TripleSDK.Ironsrc
{
    [InjectService()]
    public class IronSourceRewardedVideo : MonoBehaviour, IVideoAds
    {
        private AdsContext m_context = new AdsContext();

        public AdsContext Context => m_context;

        public Action onVideoAdsOpenedEvent { get; set; }
        public Action onVideoAdsClosedEvent { get; set;}
        public Action<string, string, int> onVideoAdRewardedEvent { get; set; }
        public Action<string> onVideoAdShowFailedEvent { get; set;}

        public AdPlacement GetPlacement(string placementName) 
        {
            var placement = IronSource.Agent.getPlacementInfo(placementName);
            //Placement can return null if the placementName is not valid.
            if(placement != null)
            {
                return new AdPlacement() {
                  PlacementName = placement.getPlacementName(),
                  RewardName =  placement.getRewardName(),
                  RewardAmount =  placement.getRewardAmount(),
                };
            }
            return null;
        }
        public string DynamicUserId { get; set; }

        public bool RewardedVideoAvailability
        {
            get;
            private set;
        }

        public void ShowRewardedVideo()
        {
            this.LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_SHOW_READY);

            IronSource.Agent.showRewardedVideo();
            if (!IronSource.Agent.isRewardedVideoAvailable())
            {
                this.LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_SHOW_NOTREADY);
                Debug.Log("unity-script: IronSource.Agent.isRewardedVideoAvailable - False");
            }

#if UNITY_EDITOR
            onVideoAdRewardedEvent?.Invoke("", "", 1);
#endif
        }

        public void ShowRewardedVideo(string adPlacement)
        {
            this.LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_SHOW_READY);

            if (IronSource.Agent.isRewardedVideoAvailable())
            {
                IronSource.Agent.showRewardedVideo(adPlacement);
            }
            else
            {
                this.LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_SHOW_NOTREADY);
                Debug.Log("unity-script: IronSource.Agent.isRewardedVideoAvailable - False");
            }
        }

        // ** STEP 1. Implement the Rewarded Video Events
        void Start()
        {
            IronSource.Agent.shouldTrackNetworkState(true);

            IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
            IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        }

        public void LogVideoAds(string eventName)
        {
             var service = Bootstrap.GetService<IAnalytics>();
            if (service == null)
            {
                Debug.LogError("Missing an analytics provider");
            }
            else
            {
               service.LogEvent(eventName, new Dictionary<string, object>  {
                    {AdsContext.SONG_ID, Context.SongId},
                    {AdsContext.SONG_NAME, Context.SongName},
                    {AdsContext.SCREEN, Context.Screen},
                    {AdsContext.LEVEL, Context.Level},
                    {AdsContext.TYPE, Context.Type},
                });
            }       
        }
        
        void OnDestroy()
        {
            IronSourceEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent -= RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent -= RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;
            IronSourceEvents.onRewardedVideoAdClickedEvent -= RewardedVideoAdClickedEvent;
        }

        protected void SafeInvolke(Action action)
        {
            if (action != null)
            {
                try
                {
                    action.Invoke();
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Ads Analytics error: " + ex.Message);
                }
            }
        }
       
        //Invoked when the RewardedVideo ad view has opened.
        //Your Activity will lose focus. Please avoid performing heavy 
        //tasks till the video ad will be closed.
        void RewardedVideoAdOpenedEvent()
        {
            LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_SHOW);
            SafeInvolke(onVideoAdsOpenedEvent);
        }

        //Invoked when the RewardedVideo ad view is about to be closed.
        //Your activity will now regain its focus.
        void RewardedVideoAdClosedEvent()
        {
            SafeInvolke(onVideoAdsClosedEvent);
        }

        //Invoked when there is a change in the ad availability status.
        //@param - available - value will change to true when rewarded videos are available. 
        //You can then show the video by calling showRewardedVideo().
        //Value will change to false when no videos are available.
        void RewardedVideoAvailabilityChangedEvent(bool available)
        {
            //Change the in-app 'Traffic Driver' state according to availability.
            RewardedVideoAvailability = available;
            Development.Log("IronSourceRewardedVideo: RewardedVideoAvailabilityChangedEvent: " + available);
        }

        //  Note: the events below are not available for all supported rewarded video 
        //   ad networks. Check which events are available per ad network you choose 
        //   to include in your build.
        //   We recommend only using events which register to ALL ad networks you 
        //   include in your build.
        //Invoked when the video ad starts playing.
        void RewardedVideoAdStartedEvent()
        {
            Development.Log("IronSourceRewardedVideo: RewardedVideoAdStartedEvent");
        }

        //Invoked when the video ad finishes playing.
        void RewardedVideoAdEndedEvent()
        {
            LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_FINISH);
            // SafeInvolke(onVideoAdEndedEvent);
        }

        //Invoked when the user completed the video and should be rewarded. 
        //If using server-to-server callbacks you may ignore this events and wait for the callback from the  ironSource server.
        //
        //@param - placement - placement object which contains the reward data
        //
        void RewardedVideoAdRewardedEvent(IronSourcePlacement ssp)
        {
            Development.Log("IronSourceRewardedVideo: RewardedVideoAdRewardedEvent");
            if (onVideoAdRewardedEvent != null)
            {
                try
                {
                    onVideoAdRewardedEvent.Invoke(
                         ssp.getPlacementName(),
                         ssp.getRewardName(),
                         ssp.getRewardAmount()
                    );
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Ads Analytics error: " + ex.Message);
                }
            }
        }

        //Invoked when the Rewarded Video failed to show
        //@param description - string - contains information about the failure.
        void RewardedVideoAdShowFailedEvent(IronSourceError error)
        {
            var code = error.getErrorCode();
            LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_SHOW_FAILED);

            if (onVideoAdShowFailedEvent != null)
            {
                try
                {
                    onVideoAdShowFailedEvent.Invoke(error.getDescription());
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Ads Analytics error: " + ex.Message);
                }
            }
        }

        private void RewardedVideoAdClickedEvent(IronSourcePlacement obj)
        {
            LogVideoAds(VideoAdsEventTypes.EVENT_VIDEOADS_CLICK);
        }
    }

}
