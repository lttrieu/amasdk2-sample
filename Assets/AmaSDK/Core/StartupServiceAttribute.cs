using System;

namespace Amanotes.TripleSDK.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class StartupServiceAttribute: Attribute 
    {

    }
}