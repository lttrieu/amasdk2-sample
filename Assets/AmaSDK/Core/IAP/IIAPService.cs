using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace Amanotes.TripleSDK.Core.IAP
{
    public delegate Task<bool> ProcessPurchase(string productId, string transactionId, string receipt, string meta);

    public interface IIAPService
    {
        void Init(IEnumerable<ProductInfo> products);

        bool IsInitialized();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        void BuyProductID(string productId);

        /// <summary>
        /// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        /// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        /// </summary>
        void RestorePurchases();

        ProcessPurchase ProcessPurchaseDelegate {get; set;}

        /// <summary>
        /// Called when a purchase fails.
        /// </summary>
        /// <value></value>
        Action<string> onPurchaseFailed { get; set; }

        /// <summary>
        /// Called when a purchase service intitialized.
        /// </summary>
        /// <value></value>
        Action<bool> onInitialized {get; set;}
    }
}