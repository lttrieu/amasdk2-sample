namespace Amanotes.TripleSDK.Core.IAP
{
    public enum StoreProductType
    {
        Consumable = 0,
        NonConsumable = 1,
        Subscription = 2
    }

    public class ProductInfo {
        public string Id { get; set; }
        public StoreProductType ProductType { get; set; }
    }
}