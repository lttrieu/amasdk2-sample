using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Linq;

namespace Amanotes.TripleSDK.Core
{
    public static class Utils
    {
        public static Type FindClass(string assemblyName, string className)
        {
            bool flag = !string.IsNullOrEmpty(assemblyName);
            string text = (!flag) ? className : (className + ", " + assemblyName);
            Type type = Type.GetType(text);
            if (type == null)
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                for (int i = 0; i < assemblies.Length; i++)
                {
                    Assembly assembly = assemblies[i];
                    if (flag)
                    {
                        if (assembly.GetName().Name == assemblyName)
                        {
                            type = Type.GetType(className + ", " + assembly.FullName);
                            break;
                        }
                    }
                    else
                    {
                        Type[] types = assembly.GetTypes();
                        for (int j = 0; j < types.Length; j++)
                        {
                            Type type2 = types[j];
                            if (type2.FullName == className)
                            {
                                type = type2;
                            }
                        }
                        if (type != null)
                        {
                            break;
                        }
                    }
                }
            }

            return type;
        }

        public static IEnumerable<Type> FindTypesWithAttribute<A>(string assemblyName = "Assembly-CSharp")
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var assembly = assemblies.First(e => e.GetName().Name == assemblyName);
            foreach (Type type in assembly.GetTypes())
            {
                if (type.GetCustomAttributes(typeof(A), true).Length > 0)
                {
                    yield return type;
                }
            }
        }
    }
}