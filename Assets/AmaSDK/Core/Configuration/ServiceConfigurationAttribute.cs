using System;

namespace Amanotes.TripleSDK.Core.Configuration
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ServiceConfigurationAttribute: Attribute 
    {
        public string Name { get; set; }

        public Type Type { get; set; }
    }
}