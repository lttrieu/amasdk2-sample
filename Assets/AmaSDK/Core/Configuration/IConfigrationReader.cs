namespace Amanotes.TripleSDK.Core.Configuration
{
    public interface IConfigrationReader {
        T GetConfiguration<T>(ServiceConfigurationAttribute attr);
    }
}