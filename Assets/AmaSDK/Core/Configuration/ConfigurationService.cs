﻿using System;
using UnityEngine;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Core.Configuration
{
    [InjectService]    
    public class ConfigurationService
    {
        public C GetConfiguration<T, C>() 
            where C: UnityEngine.Object, new()
           
        {
            C config = default(C);

            ServiceConfigurationAttribute attr =
                (ServiceConfigurationAttribute)Attribute.GetCustomAttribute(typeof(T), typeof(ServiceConfigurationAttribute));

            if(attr != null) {
                config = Resources.Load<C>(attr.Name);
                if(config == null) {
                    Debug.LogError($"Could not found {attr.Name} asset");
                }
            }
            
            return config;
        }
    }
}
