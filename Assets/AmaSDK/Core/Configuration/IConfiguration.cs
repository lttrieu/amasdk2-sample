namespace Amanotes.TripleSDK.Core.Configuration
{
    public interface IConfiguration {
        string GetString(string keyName);
    }
}