using System;

namespace Amanotes.TripleSDK.Core.AdUnits
{

    public sealed class AdsContext
    {
        /// <summary>
        /// called to show ad to continue game when user failed in game
        /// </summary>
        public const string ADS_TYPE_CONTINUE = "continue";

        /// <summary>
        /// called to show ad to receive free diamond
        /// </summary>
        public const string ADS_TYPE_DIAMOND = "getdiamond";

        /// <summary>
        /// called to show ad to receive x2 diamond 
        /// </summary>
        public const string ADS_TYPE_X2DIAMOND = "x2diamond";
        
        /// <summary>
        /// called to show ad to user unlock song
        /// </summary>
        public const string ADS_TYPE_UNLOCKSONG = "unlocksong";

        public const string SONG_ID = "song_id";
        public const string SONG_NAME = "song_name";
        public const string SCREEN = "screen";
        public const string LEVEL = "level";
        public const string TYPE = "type";

        /// <summary>
        /// Name of song where event is triggered (if song uploaded by user, value is "local_song")
        /// </summary>
        public string SongId { get; set; }
        /// <summary>
        /// if song uploaded by user, value is "local_song"
        /// </summary>
        public string SongName { get; set; }

        /// <summary>
        /// Name of screen where event is triggered
        /// With the following values:
        /// - continue_game: called to show ad in continue screen when user failed in game
        /// - result_game: called to show ad when user finish a song and in result screen
        /// - home: called to show ad when user in home screen (main menu)
        /// - shop: called to show ad when user in shop screen"
        /// </summary>
        public string Screen { get; set; }
        /// <summary>
        /// The ordinal number of the song in the song list (i.e whether it is the 1st song, 2nd....)
        /// </summary>
        public int Level { get; set; }

        public string Type { get; set; }

        

    }

}