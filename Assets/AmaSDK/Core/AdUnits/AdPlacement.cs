namespace Amanotes.TripleSDK.Core.AdUnits
{
    public class AdPlacement 
    {
        public string PlacementName { get; set; }
        public string RewardName { get; set; }
        public int RewardAmount { get; set; }
    }
}