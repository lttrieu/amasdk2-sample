using System;

namespace Amanotes.TripleSDK.Core.AdUnits
{
    public interface IInterstitialAds {
        void LoadInterstitial();
        void ShowInterstitial();
        void ShowInterstitial(string placementName);

         void LogFullAds(string eventName);

        AdsContext Context { get; }

        //Invoked when the initialization process has failed.
        //@param description - string - contains information about the failure.
        Action<string> onInterstitialAdLoadFailedEvent { get; set; }

        Action<string> onInterstitialAdShowFailedEvent { get; set; }

        Action onInterstitialAdShowSucceededEvent { get; set; }


        Action onInterstitialAdReadyEvent { get; set; }

        bool IsInterstitialReady { get; }

        /// <summary>
        /// Invoked when the interstitial ad closed and the user goes back to the application screen.
        /// </summary>
        /// <value></value>
        Action onInterstitialAdClosedEvent { get; set; }

    }
}