using System;

namespace Amanotes.TripleSDK.Core.AdUnits
{
    public interface IVideoAds {
        /// <summary>
        ///  Show a video ad to your users
        /// </summary>
        void ShowRewardedVideo();
        void ShowRewardedVideo(string adPlacement);
        bool RewardedVideoAvailability {get;}
        
        void LogVideoAds(string eventName);

        AdsContext Context { get; }

        /// <summary>
        ///  Invoked when the RewardedVideo ad view has opened.
        /// <br/>Your Activity will lose focus. Please avoid performing heavy 
        /// <br/>tasks till the video ad will be closed.
        /// </summary>
        Action onVideoAdsOpenedEvent { get; set;}

        /// <summary>
        /// Invoked when the RewardedVideo ad view is about to be closed.
        /// Your activity will now regain its focus.
        /// </summary>
        Action onVideoAdsClosedEvent { get; set;}

        /// <summary>
        /// Invoked when the user completed the video and should be rewarded.
        /// <br/> - Placement name
        /// <br/> - Reward name
        /// <br/> - Reward amount
        /// </summary>
        Action<string, string, int> onVideoAdRewardedEvent { get; set;}

        /// <summary>
        /// Invoked when the Rewarded Video failed to show
        /// @param description - string - contains information about the failure.
        /// </summary>
        Action<string> onVideoAdShowFailedEvent { get; set;}

        AdPlacement GetPlacement(string placementName);
    }
}