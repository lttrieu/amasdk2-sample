using System;

namespace Amanotes.TripleSDK.Core.Analytics
{
    public static class VideoAdsEventTypes
    {
        /// <summary>
        /// When an RW ad is requested from ads network
        /// </summary>
        // const string EVENT_VIDEOADS_REQUEST = "videoads_request";
        /// <summary>
        /// When an ad request has been sent but received no response from ads network
        /// </summary>
        // const string EVENT_VIDEOADS_REQUEST_FAILED = "videoads_request_failed";
        /// <summary>
        /// When an ad request has been sent and received response from ads network
        /// </summary>
        // const string EVENT_VIDEOADS_REQUEST_SUCCESS = "videoads_request_success";

        /// <summary>
        /// When an ad is called to show in app
        /// </summary>
        public const string EVENT_VIDEOADS_SHOW_READY = "videoads_show_ready";
        /// <summary>
        /// When an ad is called to show in app but no ad available
        /// </summary>
        public const string EVENT_VIDEOADS_SHOW_NOTREADY = "videoads_show_notready";
        /// <summary>
        /// When an ad is called to show in app and it show successfully
        /// </summary>
        public const string EVENT_VIDEOADS_SHOW = "videoads_show";
        /// <summary>
        /// When an ad is called to show in app but cannot show due to error
        /// </summary>
        public const string EVENT_VIDEOADS_SHOW_FAILED = "videoads_show_failed";
        /// <summary>
        /// When user finish watch an ad
        /// </summary>
        public const string EVENT_VIDEOADS_FINISH = "videoads_finish";
        /// <summary>
        /// When user touching on CTA button of an ad
        /// </summary>
        public const string EVENT_VIDEOADS_CLICK = "videoads_click";

        /// <summary>
        /// When an ad is called to show in app and it show successfully
        /// </summary>
        public const string EVENT_BANNERADS_SHOW = "bannerads_show";

        /// <summary>
        /// When user touching on CTA button of an ad
        /// </summary>
        public const string EVENT_BANNERADS_CLICK = "bannerads_click";
    }

}