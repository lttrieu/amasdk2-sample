using System;

namespace Amanotes.TripleSDK.Core.AdUnits
{
   public enum BannerPosition {
       TOP = 1,
	    BOTTOM = 2
   }

    public interface IBannerAds {
        void LoadBanner(BannerPosition position = BannerPosition.BOTTOM);
        void HideBanner();
        void ShowBanner();
    }
}