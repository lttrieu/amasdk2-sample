using System;

namespace Amanotes.TripleSDK.Core.Analytics
{
    public static class FullAdsTypes
    {
        public const string EVENT_FULLADS_REQUEST = "fullads_request";
        public const string EVENT_FULLADS_REQUEST_FAILED = "fullads_request_failed";
        public const string EVENT_FULLADS_REQUEST_SUCCESS = "fullads_request_success";
        public const string EVENT_FULLADS_SHOW_READY = "fullads_show_ready";
        public const string EVENT_FULLADS_SHOW_NOTREADY = "fullads_show_notready";
        public const string EVENT_FULLADS_SHOW = "fullads_show";
        public const string EVENT_FULLADS_SHOW_FAILED = "fullads_show_failed";
        public const string EVENT_FULLADS_FINISH = "fullads_finish";
        public const string EVENT_FULLADS_CLICK = "fullads_click";

    }
}
