﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Firebase;


public class LogDisplay : MonoBehaviour
{
    public Text m_text;
    private StringBuilder m_stringBuilder = new StringBuilder();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Log(string msg, string tag = "INFO")
    {
        m_stringBuilder.Insert(0, $"[{tag}] {msg}\n");
        m_text.text = m_stringBuilder.ToString();
    }

    public void LogError(string msg) {
        this.Log(msg, "ERROR");
    }

}
