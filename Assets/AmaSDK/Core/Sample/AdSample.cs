﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Core.AdUnits;
using System;

public class AdSample : MonoBehaviour
{
    public LogDisplay logger;

    // Start is called before the first frame update
    void Start()
    {

        var videoAdService = Bootstrap.GetService<IVideoAds>();
        // Invoked when the user completed the video and should be rewarded.
        videoAdService.onVideoAdRewardedEvent += VideoAdRewardedEvent;
        // Invoked when the RewardedVideo ad view is about to be closed.
        videoAdService.onVideoAdsClosedEvent += VideoAdsClosedEvent;
        // Invoked when the Rewarded Video failed to show
        videoAdService.onVideoAdShowFailedEvent += VideoAdShowFailedEvent;

        var service = Bootstrap.GetService<IInterstitialAds>();
        //Invoked when the ad fails to show.
        service.onInterstitialAdShowFailedEvent += OnAdShowFailed;
        //Invoked right before the Interstitial screen is about to open.   
        service.onInterstitialAdShowSucceededEvent += OnAdShowSuccessed;
        //Invoked when the interstitial ad closed
        service.onInterstitialAdClosedEvent += OnAdClosed;

    }

    private void OnAdClosed()
    {
        logger.Log(@"[Interstitial] Ad Closed");
    }

    private void OnAdShowSuccessed()
    {
        logger.Log(@"[Interstitial] Ad Show Successed");
    }

    //Invoked when the ad fails to show.
    private void OnAdShowFailed(string error)
    {
        logger.LogError($"[Interstitial] Ad Show Failed.\nError: {error}");
    }

    // Invoked when the user completed the video and should be rewarded.
    //  - Placement name
    //  - Reward name
    //  - Reward amount
    private void VideoAdRewardedEvent(string placement, string reward, int amount)
    {
        logger.Log($"[VideoAd RewardedEvent] placement: {placement} reward: {reward} amount {amount}");
    }

    // Invoked when the RewardedVideo ad view is about to be closed.
    private void VideoAdsClosedEvent()
    {
        logger.Log($"[VideoAds] Closed Event");
    }

    // Invoked when the Rewarded Video failed to show
    // @param description contains information about the failure.
    private void VideoAdShowFailedEvent(string error)
    {
        logger.LogError($"[VideoAds] Video Ad Show Failed.\nError: {error}");
    }

    public void ShowVideoAds()
    {
        var service = Bootstrap.GetService<IVideoAds>();
        if (service == null) {
            logger.LogError("[VideoAds] Do you forget attach Bootrap component to scene?");
            return;
        }

        if (!service.RewardedVideoAvailability) {
            logger.LogError(@"[VideoAds] Video Ads service is not available.
Please check your internet connection, ironsource key, mediation adapter.");
            return;
        }

        service.ShowRewardedVideo();
    }

    public void LoadBannerAds()
    {
        var bannerService = Bootstrap.GetService<IBannerAds>();
        if (bannerService == null)
        {
            logger.LogError("[Banner] Do you forget attach Bootrap component to scene?");
            return;
        }
        logger.Log("[Banner] Load banner and show");
        bannerService.LoadBanner();
    }

    public void HideBanner()
    {
        logger.Log("[Banner] Hide banner");
        Bootstrap.GetService<IBannerAds>().HideBanner();

    }

    public void ShowBanner()
    {
        Bootstrap.GetService<IBannerAds>().ShowBanner();
    }


    public void LoadInterstitial()
    {
        logger.Log("[Interstitial] Load Interstitial...");
        Bootstrap.GetService<IInterstitialAds>().LoadInterstitial();
    }

    public void ShowInterstitial()
    {
        logger.Log("[Interstitial] Show Interstitial..");
        Bootstrap.GetService<IInterstitialAds>().ShowInterstitial();
    }

}
