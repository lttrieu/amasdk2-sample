﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Core.Analytics;


public class AnalyticsSample : MonoBehaviour
{
    public LogDisplay logger;

    private bool ValidFirebaseAnalytics()
    {
        var analyticService = Bootstrap.GetService<IAnalytics>();
        if (analyticService == null)
        {
            logger.Log("Firebase Analytics is not available. \n Do you forget adding Bootstrap component?", "ERROR");
            return false;
        }
      
        return true;
    }

    public void LogFBEvent()
    {
        if (ValidFirebaseAnalytics())
        {
            var service = Bootstrap.GetService<IAnalytics>();
            var eventName = "fb_test_event";
            service.LogEvent(eventName);
            logger.Log($"Log custom event: {eventName}");
        }
    }

    public void LogFBEvent2()
    {
        if (ValidFirebaseAnalytics())
        {
            var service = Bootstrap.GetService<IAnalytics>();
            var eventName = "fb_test_event2";
            var paramName = "message";
            var paramValue = "hi";
            service.LogEvent(eventName, new Dictionary<string, object>() {
                {paramName, paramValue}
            });
            logger.Log($"Log custom event: {eventName} \n{paramName} = {paramValue}");
        }

    }

}
