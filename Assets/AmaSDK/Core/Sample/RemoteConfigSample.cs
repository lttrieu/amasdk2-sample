﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Amanotes.TripleSDK.Core;
using Amanotes.TripleSDK.Firebase;

public class RemoteConfigSample : MonoBehaviour
{

    public LogDisplay logger;

    public void GetRemoteConfig()
    {
        var service = Bootstrap.GetService<IIRemoteRemoteConfig>();
        var message = service.GetStringValueRemoteConfig("message");

        logger.Log($"Remote config: message={message}");

    }
}
