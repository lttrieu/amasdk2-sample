﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.TripleSDK.Core.Analytics
{
    public interface IIAPAnalytics 
    {
        IAPContext Context { get; }

        /// <summary>
        /// Trigger when user click to a button that can make theme purchase an item. 
        /// Non trigger for non-consumable item if the user already purchased that item  
        /// </summary>
        void IAPClick();

        /// <summary>
        /// Trigger when user see the button that can make them purchase an item. 
        /// Non trigger for non-consumable item if the user already purchased that item  
        /// </summary>
        void IAPImpression();

        /// <summary>
        /// Trigger when user purchase an item
        /// </summary>
        void IAPPurchased();

        /// <summary>
        /// Trigger when user purchase an item succesfully
        /// </summary>
        void IAPFailed();
    }
}