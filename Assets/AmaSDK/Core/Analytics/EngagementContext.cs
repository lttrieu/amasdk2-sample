namespace Amanotes.TripleSDK.Core.Analytics
{
    public class EngagementContext
    {
        /// <summary>
        /// Song ID: name song of mp3 file
        /// </summary>
        public string SongId { get; set; }

        /// <summary>
        /// Song Name
        /// </summary>
        public string SongName { get; set; }

        /// <summary>
        /// With the following values: 
        /// - song_list: song list in game 
        /// - local_song: song uploaded by user"
        /// </summary>
        public string SongType { get; set; }

        /// <summary>
        /// The ordinal number of the song in the song list (i.e whether it is the 1st song, 2nd....)
        /// <br/> Note: 
        /// ""level"" is only applied for songs in the game song list
        /// <br/>If the game has Upload Local Song feature, the level of uploaded songs must be different from original song list (because local-song songs will be placed in the 1st place of song list)
        /// => Level of uploaded-songs: Add a ""a"" symbol after the number. Example: 1a, 2a, 3a
        /// <br/> Apply to all ""level"" paramaters"
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// With the following values:
        /// - Songlist
        /// - Suggestion: from result scene, pop up, etc...
        /// - Replay
        /// - Others: Unidentified locations
        /// <br />Triggered at the end/finish of a new level/song, including tutorial.(Reach to 100% / 3 stars)
        /// </summary>
        public string ItemSource { get; set; }

        /// <summary>
        /// Unlock condition for the song, with the following values:
        /// - Default: unlocked automatically.
        /// - Progression: Unlocked by completing previous level and satisfying the other requirement (percentages/challenges...)
        /// - Ads: Unlocked by watching a number of reward video ads.
        /// - Currency - Soft: bought with softcurrency
        /// - Currency - Hard: bought with hard currency
        /// - Gift: songs unlocked by participating in ingame event."
        /// </summary>
        public string Unlock { get; set; }

        /// <summary>
        /// Percentage of completion, value from 0..100, can exceed 100 if the game has endless mode.
        /// </summary>
        public int Progress { get; set; }

        /// <summary>
        /// Score
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Playing time in seconds since the start of the level, not including time spent watching ads.
        /// </summary>
        /// <value></value>
        public int PlayTime { get; set; }

        /// <summary>
        /// ID of midi that user fails
        /// </summary>
        /// <value></value>
        public int NoteMidi { get; set; }

        /// <summary>
        /// Based on note_midi, class NoteData which has timeAppear
        /// </summary>
        public int PlayTimeMidi { get; set; }

        /// <summary>
        /// Amount of soft currency earned
        /// </summary>
        public int Currency { get; set; }

        /// <summary>
        /// Type of soft currency earned
        /// <br/> Triggered every time a player fail in a level (even if he would revive with reward video after), including failing after revival
        /// </summary>
        public string CurrencyType { get; set; }

        /// <summary>
        /// Name of the challenge that results in failure
        /// </summary>
        /// <value></value>
        public string Challenge { get; set; }

        /// <summary>
        /// Name of the obstacle that results in failure
        /// </summary>
        /// <value></value>
        public string Obstacle { get; set; }

        /// <summary>
        /// User are able to continue or not 
        /// - Do the continue pop-up appear after user fails(Yes/No)
        /// <br />Triggered every time a player click revive after fail in a level (even if he would revive with reward video after), including failing after revival
        /// </summary>
        public string AbleToContinue { get; set; }


    }
}