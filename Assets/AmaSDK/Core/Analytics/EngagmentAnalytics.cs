using System.Collections.Generic;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Core.Analytics
{
    public class EngagmentAnalytics : IEngagementAnalytics
    {
        const string EVENT_SONG_START = "song_start";
        const string EVENT_SONG_END = "song_end";
        const string EVENT_SONG_RESULT = "song_result";
        const string EVENT_SONG_FAILED = "song_fail";
        const string EVENT_SONG_REVIVE = "song_revive";
        const string EVENT_SONG_SUGGEST_SHOW = "song_suggest_show";
        const string EVENT_SONG_SUGGEST_CLICK = "song_suggest_click";
        const string EVENT_SONG_PREVIEW = "song_preview";
        const string EVENT_SONG_PREVIEW_CLICK = "song_preview_click";
        const string EVENT_SONG_IMPRESSION = "song_impression";
        const string EVENT_SONG_CLICK = "song_click";
        const string EVENT_SONG_AP = "song_ap";
        const string EVENT_SONG_IMPRESSION_CONTINUE = "continue_impression";

        protected virtual void LogEvent(string eventName, Dictionary<string, object> param)
        {
            var service = Bootstrap.GetService<IAnalytics>();
            service.LogEvent(eventName, param);
        }

        protected EngagementContext m_context = new EngagementContext();

        public EngagementContext Context => m_context;

        public void LogEventSongStart()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            this.LogEvent(EVENT_SONG_START, parameters);
        }

        public void LogEventSongEnd()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_END, parameters);
        }

        public void LogEventSongResult()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"unlock", Context.Unlock},
                {"progress", Context.Progress},
                {"score", Context.Score},
                {"playtime", Context.PlayTime},
                {"note_midi", Context.NoteMidi},
                {"playtime_midi", Context.PlayTimeMidi},
                {"currency", Context.Currency},
                {"type_currency", Context.CurrencyType},
            };
            LogEvent(EVENT_SONG_RESULT, parameters);
        }

        public void LogEventSongFailed()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"unlock", Context.Unlock},
                {"progress", Context.Progress},
                {"score", Context.Score},
                {"playtime", Context.PlayTime},
                {"note_midi", Context.NoteMidi},
                {"playtime_midi", Context.PlayTimeMidi},
                {"currency", Context.Currency},
                {"type_currency", Context.CurrencyType},
                {"challenge", Context.Challenge},
                {"obstacle", Context.Obstacle},
                {"AbleToContinue", Context.AbleToContinue},
            };
            LogEvent(EVENT_SONG_FAILED, parameters);
        }

        public void LogEventSongRevive()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_REVIVE, parameters);
        }

        public void LogEventSongSuggestShow()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_SUGGEST_SHOW, parameters);
        }

        public void LogEventSongSuggestClick()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_SUGGEST_CLICK, parameters);
        }

        public void LogEventSongPreview()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };

            LogEvent(EVENT_SONG_PREVIEW, parameters);
        }

        public void LogEventSongPreviewClick()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_PREVIEW_CLICK, parameters);
        }

        public void LogEventSongImpression()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_IMPRESSION, parameters);
        }

        public void LogEventSongClick()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_CLICK, parameters);
        }

        public void LogEventSongAp()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_AP, parameters);
        }

        public void LogEventContinueImpression()
        {
            var parameters = new Dictionary<string, object> {
                {"song_id", Context.SongId},
                {"song_name", Context.SongName},
                {"type_song", Context.SongType},
                {"level", Context.Level},
                {"item_source", Context.ItemSource},
                {"unlock", Context.Unlock},
            };
            LogEvent(EVENT_SONG_IMPRESSION_CONTINUE, parameters);
        }
    }
}