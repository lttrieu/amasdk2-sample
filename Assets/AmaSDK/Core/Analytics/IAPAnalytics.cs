using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.TripleSDK.Core.Analytics
{
    public class IAPAnalytics : IIAPAnalytics
    {
        private IAPContext m_context = new IAPContext();

        public IAPContext Context => m_context;

        public const string EVENT_IAP_CLICK = "iap_click";
        public const string EVENT_IAP_IMPRESSION = "iap_impression";
        public const string EVENT_IAP_PURCHASED = "iap_purchased";
        public const string EVENT_IAP_FAILED = "iap_purchased_fail";

        private void LogEvent(string eventName)
        {
            var service = Bootstrap.GetService<IAnalytics>();
            if(service == null) {
                Debug.LogError("Analytics Service is not available");
                return;
            }
            service.LogEvent(eventName, new Dictionary<string, object>
            {
                {"item_source", m_context.ItemSource},
                {"screen", m_context.Screen},
                {"order", m_context.Order},
                {"package", m_context.Package},
            });
        }

        public void IAPClick()
        {
            LogEvent(EVENT_IAP_CLICK);
        }

        public void IAPImpression()
        {
            LogEvent(EVENT_IAP_IMPRESSION);
        }

        public void IAPPurchased()
        {
            LogEvent(EVENT_IAP_PURCHASED);
        }

        public void IAPFailed()
        {
            LogEvent(EVENT_IAP_FAILED);
        }
    }
}