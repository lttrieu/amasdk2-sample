using System.Collections.Generic;

namespace Amanotes.TripleSDK.Core.Analytics
{
    public interface IAnalytics {
        
        void LogEvent(string eventName);

         void LogEvent(string eventName, Dictionary<string, object> param);
    }
}