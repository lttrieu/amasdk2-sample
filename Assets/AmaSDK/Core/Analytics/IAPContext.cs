namespace Amanotes.TripleSDK.Core.Analytics
{
    public class IAPContext
    {
        /// <summary>
        /// The name of the function which is included the IAP button
        /// ex: - Revive_pop_up 
        /// - small_diamond_shop 
        /// - large_diamond_shop
        /// - remove_ads
        /// ....
        /// </summary>
        /// <value></value>
        public string ItemSource { get; set; }

        /// <summary>
        /// The name of the screen which is included the IAP button
        ///ex:
        ///- home
        ///- result
        ///- shop
        ///- action_phase
        ///...
        /// </summary>
        /// <value></value>
        public string Screen { get; set; }

        /// <summary>
        /// The order of how many time user see that button in their life-time
        ///ex: 1,2,3,4,5,... (1st, 2nd, 3rd, 4th, 5th times...)"
        /// </summary>
        /// <value></value>
        public string Order { get; set; }

        /// <summary>
        /// The name of package that user click 
        /// ex: 
        /// - large_diamond
        /// - small_diamond
        /// - remove_ads
        /// - unlock_all
        /// ...
        /// </summary>
        /// <value></value>
        public string Package { get; set; }
    }
}