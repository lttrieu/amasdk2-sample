namespace Amanotes.TripleSDK.Core.Analytics
{
    public interface IEngagementAnalytics {
        EngagementContext Context { get; }
        /// <summary>
        /// "Triggered at the end/finish of a new level/song, including tutorial.(Reach to 100% / 3 stars)
        /// </summary>
        void LogEventSongEnd();
        /// <summary>
        /// Triggered at the start of a new level/song. Including: Action phase in tutorial, tap on Play from Main Screen, Retry (Replay). Not including: Revive to continue.
        /// </summary>
        void LogEventSongStart();
        /// <summary>
        /// Triggered when the player reaches Result Screen.
        /// </summary>
        void LogEventSongResult();
        /// <summary>
        /// Triggered every time a player fail in a level (even if he would revive with reward video after), including failing after revival
        /// </summary>
        void LogEventSongFailed();
        /// <summary>
        /// Triggered every time a player click revive after fail in a level (even if he would revive with reward video after), including failing after revival
        /// </summary>
        void LogEventSongRevive();
        /// <summary>
        /// Triggered when user click at the start of a song that suggested on in result screen
        /// </summary>
        void LogEventSongSuggestClick();
        /// <summary>
        /// Triggered when song suggested at the start of a song that suggested on in result screen
        /// </summary>
        void LogEventSongSuggestShow();
        /// <summary>
        /// Triggered when user click play button to listen a short time 
        /// (greater than 2 seconds) of song (try to know he like this song or not?) 
        /// </summary>
        void LogEventSongPreview();
        /// <summary>
        /// "Triggered after users click play button to listen (that is song_preview event), 
        ///  users click play song and triggerd event 'song_start' in the same 'song_id' with 'song_preview'
        ///  Condition: when user click preview song (event:'song_preview') 
        /// and play song (event:'song_start'), no having transfer any screen in this time"
        /// </summary>
        void LogEventSongPreviewClick();
        /// <summary>
        /// Triggered when song show on UI screen, delay 2 seconds (In home screen, result screen)
        /// </summary>
        void LogEventSongImpression();
        /// <summary>
        /// Trigger when user click PLAY button any song that they've unlocked in the game. (also count when user tap Replay)
        /// </summary>
        void LogEventSongClick();
        /// <summary>
        /// Trigger when the user finish initializes gameplay then get to Action phase screen. (also count when user tap Replay)
        /// </summary>
        void LogEventSongAp();
        /// <summary>
        /// Triggered every time a player fail in a song and screen showed 'continue' button to user choose click to continue or not
        /// </summary>
        void LogEventContinueImpression();
    }
}