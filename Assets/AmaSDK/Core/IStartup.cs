namespace Amanotes.TripleSDK.Core
{
    public interface IStartup {
        void Startup();
    }
}