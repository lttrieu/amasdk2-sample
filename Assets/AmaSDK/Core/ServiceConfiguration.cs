using System;
using UnityEngine;
using System.Linq;

namespace Amanotes.TripleSDK.Core
{
  
    [Serializable]
    public class ServiceConfigurations
    {
        [SerializeField]    
        public ServiceConfiguration[] configs;

        public bool IsEnable(string serviceName)
        {
            if (string.IsNullOrEmpty(serviceName)) return true;

            if(configs != null) {
                var config = configs.FirstOrDefault(e => e.name.Equals(serviceName, StringComparison.OrdinalIgnoreCase));
                if(config != null) return config.enable;
            }
            return true;
        }
    }

    [Serializable]
    public class ServiceConfiguration
    {
        public string name;
        public bool enable;
    }

}
