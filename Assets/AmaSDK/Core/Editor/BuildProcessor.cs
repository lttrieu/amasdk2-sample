﻿
using System.IO;
using System.Collections.Generic;

using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEngine;

using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace Amanotes.TripleSDK.Core
{
    public class BuildProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }

        public static string ANDROID_MANIFEST {
            get {
                return Path.Combine(Application.dataPath, "Plugins/Android/AndroidManifest.xml");
            }
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            if (report.summary.platform == BuildTarget.Android)
            {
                PrebuildAndroidProcessor();
            }
        }

         [MenuItem("Amanotes/AmaSDK/Android Prebuild Processor")]
        private static void PrebuildAndroidProcessor()
        {
            var path = ANDROID_MANIFEST;
            var doc = AndroidManifestHelper.LoadXmlDocument(path);
            if (doc != null)
            {
                // Support 
                
            }
            else
            {
                Debug.LogWarning($"{path} not found.");
            }
        }

        [PostProcessBuildAttribute(1)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string pathToBuiltProject)
        {
            if (buildTarget == BuildTarget.iOS)
            {
                
            }
        }
    }

}
