using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Core.Configuration
{
    [InitializeOnLoad]
    public class ConfigurationEditor
    {        
        private static List<ServiceConfigurationAttribute> s_reimportTypes = new List<ServiceConfigurationAttribute>();

        private static List<ServiceConfigurationAttribute> s_configurationAttrs;

        static ConfigurationEditor()
        {
            var types = Utils
                .FindTypesWithAttribute<ServiceConfigurationAttribute>();

            var filterTypes = types
                .GroupBy(type => ((ServiceConfigurationAttribute)Attribute.GetCustomAttribute(type, typeof(ServiceConfigurationAttribute))).Name)
                .Select(group => group.First()).ToList();

            s_configurationAttrs = (from type in filterTypes
            select (ServiceConfigurationAttribute)
                    Attribute.GetCustomAttribute(type, typeof(ServiceConfigurationAttribute))).ToList();

            StartFileWatcher(Application.dataPath);
        }

        [MenuItem("Amanotes/Import Configurations")]
        public static void Import()
        {
            foreach (var attr in s_configurationAttrs)
            {
                 Import(attr);
            }

            AssetDatabase.Refresh();
        }

        private static void StartFileWatcher(string path)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path; 

            // Watch for changes in LastAccess and LastWrite times, and
            // the renaming of files or directories.
            watcher.NotifyFilter = NotifyFilters.LastAccess
                                | NotifyFilters.LastWrite
                                | NotifyFilters.FileName
                                | NotifyFilters.DirectoryName;

            // Only watch text files.
            watcher.Filter = "*.json";

            // Add event handlers.
            watcher.Changed += OnChanged;
            watcher.Created += OnChanged;
            // watcher.Renamed += OnRenamed;

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }


        private static string GetConfigurationFileName(ServiceConfigurationAttribute attr) {
            return $"{attr.Name}.json".ToLower();
        }


        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            var filename = Path.GetFileName(e.FullPath);
            foreach (var attr in s_configurationAttrs)
            {
                if(GetConfigurationFileName(attr).Equals(filename, StringComparison.OrdinalIgnoreCase))
                {
                    EditorCoroutine.start(LazyImport(attr));
                    break;
                }
            }
        }

        private static IEnumerator LazyImport(ServiceConfigurationAttribute attr)
        {
            yield return null;
            Import(attr);
        }

        static void Import(ServiceConfigurationAttribute attr) {
           
            string[] guids;
            try
            {
                 guids = AssetDatabase.FindAssets($"{attr.Name} t:TextAsset ");
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
                throw;
            }
           

            var files = (from guid in guids
                    select AssetDatabase.GUIDToAssetPath(guid)).ToList();

            files = (from path in files
                    where Path.GetFileName(path).Equals($"{attr.Name}.json", StringComparison.OrdinalIgnoreCase)
                    select path).ToList();

            if(files.Count == 0) {
                Debug.LogError($"Configuration {attr.Name}.json does not found");
            } else if(files.Count > 1) {
                Debug.LogError($"There are {files.Count} {attr.Name}.json in assets folder.");
                foreach (var path in files)
                {
                    Debug.LogError(path);
                }
            } else { 
                var baseDir = Path.Combine(Application.dataPath, "Resources"); 
                if(!Directory.Exists(baseDir)) {
                    Directory.CreateDirectory(baseDir);
                } 
  
                var rootDir = Directory.GetParent(Application.dataPath).FullName;
                var file = files[0];
                var sourceFile = Path.Combine(rootDir, file);
                var text = File.ReadAllText(sourceFile);

                // var instance = Activator.CreateInstance(attr.Type);
                var instance = ScriptableObject.CreateInstance(attr.Type.FullName);
                JsonUtility.FromJsonOverwrite(text, instance);
         
                var scriptable = instance as ScriptableObject;
                var destFile = $"Assets/Resources/{attr.Name}.asset";
                AssetDatabase.CreateAsset(scriptable, destFile);
                AssetDatabase.SaveAssets();
                Debug.Log("Generate " + destFile);
            }

           
        }
    }
}