﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Amanotes.TripleSDK.Editor
{

    public class PackageExporter : EditorWindow
    {

        // [MenuItem("Amanotes/Export/AmaSDKCore")]
        // private static void ShowWindow() {
        //     var window = GetWindow<PackageExporter>();
        //     window.titleContent = new GUIContent("PackageExporter");
        //     window.Show();
        // }
      
        public static void ExportPackage(PackageInfo packageInfo) {
            
            var outputDir = Path.Combine(Application.dataPath, $"../{packageInfo.exportDir}");

            if(!Directory.Exists(outputDir)) {
                Directory.CreateDirectory(outputDir);
            }

            var fileName = $"{packageInfo.packageName}_v{packageInfo.version}.unitypackage";
            if(!string.IsNullOrEmpty(packageInfo.exportDir)) {
                fileName = Path.Combine(packageInfo.exportDir, fileName);
            }

            AssetDatabase.ExportPackage(packageInfo.assetPathNames, fileName, packageInfo.options);
            Debug.Log("Export " + fileName);
        }

        private void OnGUI()
        {

        }
    }
}
