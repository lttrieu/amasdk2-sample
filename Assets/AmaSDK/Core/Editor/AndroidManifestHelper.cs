using System.Xml;
using System.IO;
using System.Collections.Generic;

namespace Amanotes.TripleSDK.Core
{
    public class AndroidManifestHelper
    {
        public const string PERMISSION_INTERNET = "android.permission.INTERNET";
        public const string PERMISSION_ACCESS_NETWORK_STATE = "android.permission.ACCESS_NETWORK_STATE";
        public const string PERMISSION_ACCESS_WIFI_STATE = "android.permission.ACCESS_WIFI_STATE";
        public const string PERMISSION_ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";
        public const string PERMISSION_ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
        public const string ANDROID_URI = "http://schemas.android.com/apk/res/android";

        public static XmlNamespaceManager CreateAndroidNsmgr(XmlDocument document)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(document.NameTable);
            nsmgr.AddNamespace("android", ANDROID_URI);
            return nsmgr;
        }

        public static void AddManifestMeta(XmlDocument document, string metaType,
            string name, Dictionary<string, string> attributes = null)
        {
            XmlNamespaceManager nsmgr = CreateAndroidNsmgr(document);

            var xpath = $"/manifest/{metaType}[@android:name='{name}']";
            var metaNode = document.SelectSingleNode(xpath, nsmgr);
            if (metaNode == null)
            {
                metaNode = document.CreateNode(XmlNodeType.Element, metaType, "");
                XmlAttribute nameAttr = document.CreateAttribute("android", "name", ANDROID_URI);
                nameAttr.Value = name;
                metaNode.Attributes.SetNamedItem(nameAttr);

                var manifest = document.SelectSingleNode($"/manifest");
                var applicationNode = document.SelectSingleNode($"/manifest/application");
                manifest.InsertBefore(metaNode, applicationNode);
                manifest.InsertAfter(CreateLineBreak(document), metaNode);
            }

            if (attributes != null)
            {
                foreach (var item in attributes)
                {
                    XmlAttribute attr = document.CreateAttribute("android", item.Key, ANDROID_URI);
                    attr.Value = item.Value;
                    metaNode.Attributes.SetNamedItem(attr);
                }
            }

        }
        public static void AddPermission(XmlDocument document, string permission)
        {
            AddManifestMeta(document, "uses-permission", permission);
        }

        public static void AddUseLibrary(XmlDocument document, string name, Dictionary<string, string> attributes = null)
        {
            AddComponent(document, "uses-library", name, attributes);
        }

        public static void AddActivity(XmlDocument document, string name, Dictionary<string, string> attributes = null)
        {
            AddComponent(document, "activity", name, attributes);
        }

        public static void AddComponent(XmlDocument document, string componentType, string name,
            Dictionary<string, string> attributes = null)
        {
            XmlNamespaceManager nsmgr = CreateAndroidNsmgr(document);

            var xpath = $"/manifest/application/{componentType}[@android:name='{name}']";
            var compNode = document.SelectSingleNode(xpath, nsmgr);
            if (compNode == null)
            {
                compNode = document.CreateNode(XmlNodeType.Element, componentType, "");
                XmlAttribute nameAttr = document.CreateAttribute("android", "name", ANDROID_URI);
                nameAttr.Value = name;
                compNode.Attributes.SetNamedItem(nameAttr);

                var applicationNode = document.SelectSingleNode($"/manifest/application");
                applicationNode.AppendChild(compNode);
                applicationNode.AppendChild(CreateLineBreak(document));
            }

            if (attributes != null)
            {
                foreach (var item in attributes)
                {
                    XmlAttribute attr = document.CreateAttribute("android", item.Key, ANDROID_URI);
                    attr.Value = item.Value;
                    compNode.Attributes.SetNamedItem(attr);
                }
            }
        }

        public static void AddService(XmlDocument document, string name, Dictionary<string, string> attributes = null)
        {
            AddComponent(document, "service", name, attributes);
        }

        private static XmlNode CreateLineBreak(XmlDocument doc)
        {
            return doc.CreateTextNode("\n");

        }
        public static void AddApplicationMetaData(XmlDocument root, string name, string value)
        {
            XmlNamespaceManager nsmgr = CreateAndroidNsmgr(root);

            var xpath = $"/manifest/application/meta-data[@android:name='{name}']";
            var appIdNode = root.SelectSingleNode(xpath, nsmgr);
            if (appIdNode == null)
            {
                appIdNode = root.CreateNode(XmlNodeType.Element, "meta-data", "");
                XmlAttribute nameAttr = root.CreateAttribute("android", "name", ANDROID_URI);
                nameAttr.Value = name;
                appIdNode.Attributes.SetNamedItem(nameAttr);

                var applicationNode = root.SelectSingleNode($"/manifest/application");
                applicationNode.AppendChild(appIdNode);
                applicationNode.AppendChild(CreateLineBreak(root));
            }

            XmlAttribute newAttr = root.CreateAttribute("android", "value", ANDROID_URI);
            newAttr.Value = value;
            appIdNode.Attributes.SetNamedItem(newAttr);
        }

        public static void AddApplicationAttribute(XmlDocument root, string name, string value)
        {
            XmlNamespaceManager nsmgr = CreateAndroidNsmgr(root);

            var xpath = $"/manifest/application";
            var appIdNode = root.SelectSingleNode(xpath, nsmgr);
           
            XmlAttribute attr = root.CreateAttribute("android", name, ANDROID_URI);
            attr.Value = value;
            appIdNode.Attributes.SetNamedItem(attr);

        }

        public static XmlDocument LoadXmlDocument(string path)
        {
            XmlDocument doc = null;

            if (File.Exists(path))
            {
                doc = new XmlDocument();
                doc.PreserveWhitespace = true;
                doc.Load(path);
            }

            return doc;
        }

        public static void SaveXml(XmlDocument document, string path)
        {
            // XmlTextWriter tw = new XmlTextWriter(path, System.Text.ASCIIEncoding.UTF8);
            // tw.Formatting = Formatting.Indented;
            // document.Save(tw);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.ASCIIEncoding.UTF8;
            // settings.Indent = true;

            // settings.NewLineHandling = NewLineHandling.Replace;;
            // settings.NewLineOnAttributes = true;

            using (var wt = XmlWriter.Create(path, settings))
            {
                document.Save(wt);
            }
        }


    }
}