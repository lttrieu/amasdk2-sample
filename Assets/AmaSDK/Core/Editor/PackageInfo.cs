﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Amanotes.TripleSDK.Editor
{
    [CreateAssetMenu(fileName = "PackageInfo", menuName = "Amanotes/Create PackageInfo", order = 0)]
    public class PackageInfo : ScriptableObject
    {
        public string packageName;

        public string version;

        public string[] assetPathNames;

        public string exportDir = "Exports";

        public ExportPackageOptions options;

        [ContextMenu("Export Package")]
        void BuildPackage()
        {
            PackageExporter.ExportPackage(this);
        }
    }
}