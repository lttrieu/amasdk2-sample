using UnityEditor;

namespace Amanotes.TripleSDK.Core.Configuration
{
    public class EditorConfigurationReader : IConfigrationReader
    {
        public T GetConfiguration<T>(ServiceConfigurationAttribute attr)
        {
            T config = default(T);
            if(attr != null) {
                var guids = AssetDatabase.FindAssets($"{attr.Name} t:TextAsset");
                foreach (var guid in guids)
                {
                    var path = AssetDatabase.GUIDToAssetPath(guid);
                }
            }
            return config;
        }
    }
}