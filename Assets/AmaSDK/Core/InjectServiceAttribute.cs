using System;

namespace Amanotes.TripleSDK.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectServiceAttribute: Attribute 
    {
        public int Order { get; set; } = 0;

        [Obsolete]
        public bool IsMonobehavior { get; set; } = false;

        public int TargetPlatform { get; set; } = -1;

        public bool SingleOnTop { get; set; } = false;

        public bool Enable { get; set; } = true;

        public string Package {get; set;}
    }
}