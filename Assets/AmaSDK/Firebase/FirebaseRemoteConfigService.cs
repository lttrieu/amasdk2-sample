using System;
using UnityEngine;
using Firebase.RemoteConfig;
using System.Threading.Tasks;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Firebase
{
    [InjectService(Package = "AmaFirebase_Dotnet4")]
    [StartupService]
    public class FirebaseRemoteConfigService : FirebaseService, IIRemoteRemoteConfig
    {
        public static Action<bool> onRemoteConfigFetchComplete;

        public float remoteConfigFetchTimeExpire = 0;

        private bool m_enable;
        public override bool Enable
        {
            get => m_enable;
            set
            {
                m_enable = value;
                if (m_enable)
                {
                    var settings = FirebaseRemoteConfig.Settings;
                    //settings.IsDeveloperMode = true; //TODO: review
                    //FirebaseRemoteConfig.Settings = settings;
                    FetchRemoteDataData();
                    Development.Log("RemoteConfig configured and ready!");
                }
            }
        }

        public int GetIntValueRemoteConfig(string name)
        {
            return (int)FirebaseRemoteConfig.GetValue(name).LongValue;
        }

        public float GetFloatValueRemoteConfig(string name)
        {
            return (float)FirebaseRemoteConfig.GetValue(name).DoubleValue;
        }

        public string GetStringValueRemoteConfig(string name)
        {
            return FirebaseRemoteConfig.GetValue(name).StringValue;
        }

        public bool GetBoolValueRemoteConfig(string name)
        {
            return FirebaseRemoteConfig.GetValue(name).BooleanValue;
        }

        public void FetchRemoteDataData(bool isTest = false)
        {

            Development.Log("RemoteConfig Fetching data...");
            // FetchAsync only fetches new data if the current data is older than the provided
            // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
            // By default the timespan is 12 hours, and for production apps, this is a good
            // number.  For this example though, it's set to a timespan of zero, so that
            // changes in the console will always show up immediately.
            TimeSpan timeExpire = TimeSpan.FromHours(remoteConfigFetchTimeExpire);
            System.Threading.Tasks.Task fetchTask = FirebaseRemoteConfig.FetchAsync(
                timeExpire);
            fetchTask.ContinueWith(FetchRemoteComplete);
        }

        private void FetchRemoteComplete(Task fetchTask)
        {
            bool isError = true;
            if (fetchTask.IsCanceled)
            {
                Debug.Log("RemoteConfig Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("RemoteConfig Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                isError = false;
            }
            Bootstrap.Instance.GetRequiredService<DoOnMainThread>().QueueOnMainThread(() =>
            {
                if (isError)
                {

                    Development.Log("Firebase RemoteConfig Fetch Error");

                    if (onRemoteConfigFetchComplete != null)
                    {
                        onRemoteConfigFetchComplete(!isError);
                    }
                }
                else
                {
                    Development.Log("Firebase RemoteConfig Fetch completed successfully!, Developer to do with Firebase Remote Config Here");
                    var info = FirebaseRemoteConfig.Info;
                    switch (info.LastFetchStatus)
                    {
                        case LastFetchStatus.Success:
                            FirebaseRemoteConfig.ActivateFetched();
                            Development.Log(string.Format("RemoteConfig Remote data loaded and ready (last fetch time {0}).",
                                                info.FetchTime));
                            break;
                        case LastFetchStatus.Failure:
                            switch (info.LastFetchFailureReason)
                            {
                                case FetchFailureReason.Error:
                                    Development.Log("RemoteConfig Fetch failed for unknown reason");
                                    break;
                                case FetchFailureReason.Throttled:
                                    Development.Log("RemoteConfig Fetch throttled until " + info.ThrottledEndTime);
                                    break;
                            }
                            break;
                        case LastFetchStatus.Pending:
                            Development.Log("RemoteConfig Latest Fetch call still pending.");
                            break;
                    }
                    if (onRemoteConfigFetchComplete != null)
                    {
                        onRemoteConfigFetchComplete(!isError);
                    }
                }

            });
        }

        public void DisplayAllKeys()
        {
            Development.Log("RemoteConfig Current Keys:");

            System.Collections.Generic.IEnumerable<string> keys =
                FirebaseRemoteConfig.Keys;
            foreach (string key in keys)
            {
                Development.Log("Firebase Remotes Key:" + key);
            }
        }

    }
}