using Firebase.Messaging;

namespace Amanotes.TripleSDK.Firebase
{
    public interface IFirebaseMessaging {
        bool Enable { get; set; }

        void OnMessageReceived(object sender, MessageReceivedEventArgs e);

        void OnTokenReceived(object sender, TokenReceivedEventArgs token);
    }
}