using System.Threading.Tasks;

namespace Amanotes.TripleSDK.Firebase
{
    public interface IIRemoteRemoteConfig {
        bool Enable { get; set; }

        int GetIntValueRemoteConfig(string name);

        float GetFloatValueRemoteConfig(string name);

        string GetStringValueRemoteConfig(string name);

        bool GetBoolValueRemoteConfig(string name);

        void FetchRemoteDataData(bool isTest = false);

    }
}