using Firebase.Analytics;
using System.Collections.Generic;
using Amanotes.TripleSDK.Core.Analytics;

namespace Amanotes.TripleSDK.Firebase
{
    public interface IFirebaseAnalytics: IAnalytics {
        bool Enable {get; set;}

        void LogEvent(string eventName, params Parameter[] para);
    }
}