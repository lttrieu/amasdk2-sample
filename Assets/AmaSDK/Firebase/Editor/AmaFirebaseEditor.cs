﻿using UnityEditor;
using System.Reflection;
using System;
using System.Collections.Generic;


namespace Amanotes.PackageManager
{
    public class AmaFirebaseEditor : EditorWindow
    {
        static Type FindClass(string assemblyName, string className) {
            bool flag = !string.IsNullOrEmpty(assemblyName);
			string text = (!flag) ? className : (className + ", " + assemblyName);
            Type type = Type.GetType(text);
			if (type == null)
			{
				Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
				for (int i = 0; i < assemblies.Length; i++)
				{
					Assembly assembly = assemblies[i];
					if (flag)
					{
						if (assembly.GetName().Name == assemblyName)
						{
							type = Type.GetType(className + ", " + assembly.FullName);
							break;
						}
					}
					else
					{
						Type[] types = assembly.GetTypes();
						for (int j = 0; j < types.Length; j++)
						{
							Type type2 = types[j];
							if (type2.FullName == className)
							{
								type = type2;
							}
						}
						if (type != null)
						{
							break;
						}
					}
				}
			}

            return type;
        }

        private static Type VersionHandlerImpl
		{
			get
			{
				return FindClass("Google.VersionHandlerImpl", "Google.VersionHandlerImpl");
			}
		}

        
		public static object InvokeVersionHandlerStaticMethod(Type type, string methodName, object[] args, Dictionary<string, object> namedArgs = null)
		{
			return InvokeVersionHandlerMethod(type, null, methodName, args, namedArgs);
		}

		public static object InvokeVersionHandlerMethod(Type type, object objectInstance, string methodName, object[] args, Dictionary<string, object> namedArgs = null)
		{
			Type[] array = null;
			if (args != null && args.Length > 0)
			{
				array = new Type[args.Length];
				for (int i = 0; i < args.Length; i++)
				{
					array[i] = args[i].GetType();
				}
			}
			MethodInfo methodInfo = (array == null) ? type.GetMethod(methodName) : type.GetMethod(methodName, array);
			ParameterInfo[] parameters = methodInfo.GetParameters();
			int num = parameters.Length;
			object[] array2 = new object[num];
			int num2 = (args == null) ? 0 : args.Length;
			ParameterInfo[] array3 = parameters;
			for (int j = 0; j < array3.Length; j++)
			{
				ParameterInfo parameterInfo = array3[j];
				int position = parameterInfo.Position;
				if (position < num2)
				{
					array2[position] = args[position];
				}
				else
				{
					object obj = parameterInfo.RawDefaultValue;
					object obj2;
					if (namedArgs != null && namedArgs.TryGetValue(parameterInfo.Name, out obj2))
					{
						obj = obj2;
					}
					array2[position] = obj;
				}
			}
			return methodInfo.Invoke(objectInstance, array2);
		}

        private static object InvokeVersionHandlerMethod(string methodName, object[] args = null, 
            Dictionary<string, object> namedArgs = null)
		{
			Type bootStrappedImpl = VersionHandlerImpl;
			return InvokeVersionHandlerStaticMethod(bootStrappedImpl, methodName, args, namedArgs);
		}

        
        private static Type GooglePlayServicesImpl
		{
			get
			{
				return FindClass("Google.JarResolver", "GooglePlayServices.PlayServicesResolver");
			}
		}
        
        private static object InvokeGooglePlayServicesMethod(string methodName, object[] args = null, 
            Dictionary<string, object> namedArgs = null)
		{
			Type serviceIml = GooglePlayServicesImpl;
			return InvokeVersionHandlerStaticMethod(serviceIml, methodName, args, namedArgs);
		}
    
        [MenuItem("Amanotes/Firebase/Update Now")]
        public static void UpdateVersion() {
            InvokeVersionHandlerMethod("UpdateNow", null, null);
        }

        [MenuItem("Amanotes/Firebase/Force Resolve")]
        public static void ForceResolve() {
            InvokeGooglePlayServicesMethod("MenuForceResolve", null, null);
        }

        //  [MenuItem("Amanotes/Firebase/Resolve")]
        // public static void Resolve() {
        //     InvokeGooglePlayServicesMethod("MenuResolve", null, null);
        // }
    }

}
