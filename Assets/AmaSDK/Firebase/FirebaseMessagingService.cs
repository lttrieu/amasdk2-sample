using Firebase.Messaging;
using UnityEngine;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Firebase
{
    [InjectService(Package = "AmaFirebase_Dotnet4")]
    [StartupService]
    public class FirebaseMessagingService : FirebaseService, IFirebaseMessaging
    {
        private string topic = "TestTopic";

        public virtual void OnMessageReceived(object sender, MessageReceivedEventArgs e)
        {
            //Debug.Log("Received a new message");
            var notification = e.Message.Notification;
            if (notification != null)
            {
                Debug.Log("title: " + notification.Title);
                Debug.Log("body: " + notification.Body);
            }
            if (e.Message.From.Length > 0)
                Debug.Log("from: " + e.Message.From);
            if (e.Message.Link != null)
            {
                Debug.Log("link: " + e.Message.Link.ToString());
            }
            if (e.Message.Data.Count > 0)
            {
                Debug.Log("data:");
                foreach (System.Collections.Generic.KeyValuePair<string, string> iter in
                        e.Message.Data)
                {
                    Debug.Log("  " + iter.Key + ": " + iter.Value);
                }
            }
        }

        public virtual void OnTokenReceived(object sender, TokenReceivedEventArgs token)
        {
            Debug.Log("Firebase Received Registration Token: " + token.Token);
        }

        private bool m_enable;
        public override bool Enable
        {
            get => m_enable;
            set
            {
                m_enable = value;
                if (m_enable)
                {
                    FirebaseMessaging.SubscribeAsync(topic);
                    Development.Log("Firebase Messaging Initialized");
                }
            }
        }

        void OnEnable()
        {
            FirebaseMessaging.TokenRegistrationOnInitEnabled = true;
            FirebaseMessaging.MessageReceived += OnMessageReceived;
            FirebaseMessaging.TokenReceived += OnTokenReceived;
        }

        void OnDisable()
        {
            FirebaseMessaging.MessageReceived -= OnMessageReceived;
            FirebaseMessaging.TokenReceived -= OnTokenReceived;
        }

        void OnDestroy()
        {
            FirebaseMessaging.MessageReceived -= OnMessageReceived;
            FirebaseMessaging.TokenReceived -= OnTokenReceived;
        }
    }
}