using UnityEngine;
using System.Collections;
using Amanotes.TripleSDK.Core;
using Firebase;

namespace Amanotes.TripleSDK.Firebase
{
    public abstract class FirebaseService: MonoBehaviour {
        
        public void Startup()
        {
            StartCoroutine(LazyStartup());
        }

        IEnumerator LazyStartup()
        {
            var service = Bootstrap.Instance.GetRequiredService<GoogleServiceChecker>();
            while(!service.IsFinishedChecking)
            {
                yield return null;
            }

            if(service.DependencyStatus == DependencyStatus.Available)
            {
                Enable = true;
            }
        }

        public abstract bool Enable { get; set; }
    }
}