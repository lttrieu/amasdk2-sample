﻿using UnityEngine;
using Firebase;
using Amanotes.TripleSDK.Core;

namespace Amanotes.TripleSDK.Firebase
{
    [InjectService(Package = "AmaFirebase_Dotnet4")]
    [StartupService]
    public class GoogleServiceChecker : MonoBehaviour, IStartup
    {
        public DependencyStatus DependencyStatus { get; private set; } = DependencyStatus.UnavailableOther;

        public bool IsFinishedChecking {
            get; private set;
        }

        public void Startup()
        {
            #if UNITY_ANDROID
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                DependencyStatus = task.Result;
                IsFinishedChecking = true;

                if (DependencyStatus != DependencyStatus.Available)
                {
                    Debug.LogError(
                    "Could not resolve all Firebase dependencies: " + DependencyStatus);
                }
            });
#else
               IsFinishedChecking = true;
            DependencyStatus = DependencyStatus.Available;
#endif
        }
    }
}