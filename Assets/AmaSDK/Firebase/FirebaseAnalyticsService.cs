
using System;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using Amanotes.TripleSDK.Core;


namespace Amanotes.TripleSDK.Firebase
{
    [InjectService(Order = 0, Package = "AmaFirebase_Dotnet4")]
    [StartupService]
    public class FirebaseAnalyticsService : FirebaseService, IFirebaseAnalytics, IStartup
    {
        private const string amaSession = "ama_open_session";

        private bool m_isEnable = false;
      
        
        public override bool Enable
        {
            get
            {
                return m_isEnable;
            }
            set
            {
                m_isEnable = value;
                if(m_isEnable) {
                    FirebaseAnalytics.SetAnalyticsCollectionEnabled(m_isEnable);
                    Debug.Log("Enabling FIREBASE ANALYTIC.");
                    LogEvent("fb_analytics_initialize");
                    Bootstrap.Instance.GetRequiredService<DoOnMainThread>().QueueOnMainThread(() =>
                    {
                        int session = PlayerPrefs.GetInt(amaSession, 0);
                        session++;
                        PlayerPrefs.SetInt(amaSession, session);
                        FirebaseAnalytics.SetUserProperty(amaSession, session.ToString());
                    });
                }
            }
        }


        public void LogEvent(string eventName)
        {
            eventName = ValidateFirebaseName(eventName);
            try
            {
                if (this.Enable)
                {
                    FirebaseAnalytics.LogEvent(eventName);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("Firebase LogEvent Exception:" + ex.Message);
            }
        }

        public void LogEvent(string eventName, params Parameter[] para)
        {
            if(Debug.isDebugBuild) Debug.Log(eventName);
            
            eventName = ValidateFirebaseName(eventName);
            try
            {
                if (this.Enable)
                {
                    FirebaseAnalytics.LogEvent(eventName, para);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("Firebase LogEvent Exception:" + ex.Message);
            }
        }

        string ValidateFirebaseName(string inputStr)
        {
            string outputStr = inputStr.Replace(' ', '_');
            outputStr = outputStr.Replace('-', '_');
            return outputStr;
        }

        public void LogEvent(string eventName, Dictionary<string, object> para = null)
        {
            eventName = ValidateFirebaseName(eventName);
            if (para == null || para.Count < 1)
            {
                LogEvent(eventName);
            }
            else
            {
                List<Parameter> list = new List<Parameter>();
                foreach (KeyValuePair<string, object> pair in para)
                {
                    if(pair.Value != null) {
                        if (pair.Value is long)
                        {
                            list.Add(new Parameter(ValidateFirebaseName(pair.Key), (long)pair.Value));
                        }
                        else if (pair.Value is float || pair.Value is double)
                        {
                            list.Add(new Parameter(ValidateFirebaseName(pair.Key), (double)pair.Value));
                        }
                        else
                        {
                            list.Add(new Parameter(ValidateFirebaseName(pair.Key), pair.Value.ToString()));
                        }
                    } else {
                        Debug.LogError($"Parameter {pair.Key} is null");
                    }
                }
                LogEvent(eventName, list.ToArray());
            }
        }

        
    }
}