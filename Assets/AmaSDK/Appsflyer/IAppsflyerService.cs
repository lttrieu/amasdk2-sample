using System.Collections.Generic;
using Amanotes.TripleSDK.Core.Analytics;

namespace  Amanotes.TripleSDK.Appsflyer
{
    public interface IAppsflyerService: IAnalytics {
         void Init(string devKey, string ituneId);
         
         void AppsFlyerTrackerCallbacks();
    }
}