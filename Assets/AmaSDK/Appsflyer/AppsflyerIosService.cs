using UnityEngine;
using Amanotes.TripleSDK.Core;

namespace  Amanotes.TripleSDK.Appsflyer
{
    [InjectService(TargetPlatform = (int)RuntimePlatform.IPhonePlayer, Package = "AmaAppsflyers")]
    [StartupService]
    public class AppsflyerIosService : AppsflyerService
    {
        public override void AppsFlyerTrackerCallbacks()
        {
            LogEvent("af_ios_conversion");
        }

        public override void Init(string devKey, string appId)
        {
            AppsFlyer.setAppsFlyerKey (devKey);
            AppsFlyer.setAppID(appId);
            AppsFlyer.getConversionData ();
            AppsFlyer.trackAppLaunch ();
            IsInitialized = true;

            LogEvent("af_open_session");
        }

    }
}