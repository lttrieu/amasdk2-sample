﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Xml;
using System.Xml.Linq;

namespace Amanotes.TripleSDK.Appsflyer
{
    [InitializeOnLoad]
    public class AppsflyerIntegration : EditorWindow
    {
        const string currentVersion = "v4.20.3";

        static AppsflyerIntegration() {
            AndroidConfig_V4_20_3();
            iOSConfig_V4_20_3();
            CreateDefaultAppsflyerConfig();
        } 

        private static void iOSConfig_V4_20_3()
        {
           
        }

        private static void AndroidConfig_V4_20_3()
        { 
            var pluginDir = Path.Combine(Application.dataPath, "Plugins/Android/Appsflyer");
            if(!Directory.Exists(pluginDir)) {
                Directory.CreateDirectory(pluginDir);
            }
            
            var receiverSnipet = @"
           <!-- receiver should be inside the <application> tag -->
           <receiver android:name='com.appsflyer.MultipleInstallBroadcastReceiver' android:exported='true'>
                <intent-filter>
                    <action android:name='com.android.vending.INSTALL_REFERRER' />
                </intent-filter>
            </receiver>
           ";
           
           var permissionsSnipet = @"
             <!-- Mandatory permission: -->
            <uses-permission android:name='android.permission.ACCESS_NETWORK_STATE' />
            <uses-permission android:name='android.permission.ACCESS_WIFI_STATE' />
            <uses-permission android:name='android.permission.INTERNET' />
            <!-- Optional : -->
            <uses-permission android:name='android.permission.READ_PHONE_STATE' />
           ";

            var androidManifestFile = Path.Combine(pluginDir, "AndroidManifest.xml");
            if(!File.Exists(androidManifestFile)) {
                var manifest = $@"
<?xml version='1.0' encoding='utf-8'?>
<manifest xmlns:android='http://schemas.android.com/apk/res/android'
          xmlns:tools='http://schemas.android.com/tools'>
  
        {permissionsSnipet}

        <application>
        {receiverSnipet}
        </application>

</manifest>"; 
                File.WriteAllText(androidManifestFile, manifest);
            }

            // AssetDatabase.Refresh();
        }

        private static void CreateDefaultAppsflyerConfig()
        {
            var file = Path.Combine(Application.dataPath, "appsflyer.json");
            if (!File.Exists(file)) {
                var content = @"
{
    ""devKey"": ""nooRvfQi6Eqc5pzScifXxVnooRvfQi6Eqc5pzScifXxV"",
    ""appId"": ""[Appsflyers-AppID-iOSOnly]""
}
            ";
                File.WriteAllText(file, content);
            }
        }
    }

}

