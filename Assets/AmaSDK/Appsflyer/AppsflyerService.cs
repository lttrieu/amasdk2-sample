using System.Collections.Generic;
using Amanotes.TripleSDK.Core.Configuration;
using Amanotes.TripleSDK.Core;
using UnityEngine;

namespace Amanotes.TripleSDK.Appsflyer
{
    [ServiceConfiguration(Name = "appsflyer", Type = typeof(AppsflyerConfiguration))]
    public abstract class AppsflyerService : MonoBehaviour, IAppsflyerService, IStartup
    {
        public AppsflyerConfiguration configuration;

        public bool IsInitialized { get; protected set; }

        public virtual void LogEvent(string eventName)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param["af_count"] = "1";
            AppsFlyer.trackRichEvent(eventName, param);
        }

        public virtual void LogEvent(string eventName, Dictionary<string, object> param)
        {
            var param2 = new Dictionary<string, string>();
            foreach (var k in param.Keys)
            {
                param2[k] = param[k].ToString();
            }
            AppsFlyer.trackRichEvent(eventName, param2);
        }

        public void Startup()
        {
            this.configuration = Bootstrap.Instance.GetRequiredService<ConfigurationService>()
                .GetConfiguration<AppsflyerService, AppsflyerConfiguration>();

            Init(configuration.devKey, configuration.appId);
        }

        public abstract void Init(string devKey, string appId);

        public abstract void AppsFlyerTrackerCallbacks();
    }
}