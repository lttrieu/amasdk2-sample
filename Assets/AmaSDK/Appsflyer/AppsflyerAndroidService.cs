#if UNITY_ANDROID
using UnityEngine;
using Amanotes.TripleSDK.Core;
using System.Collections.Generic;

namespace  Amanotes.TripleSDK.Appsflyer
{
    [InjectService(TargetPlatform = (int)RuntimePlatform.Android, Package = "AmaAppsflyers")]
    [StartupService]
    public class AppsflyerAndroidService : AppsflyerService
    {
        public override void Init(string devKey, string appId)
        {
            // Debug.Log("Appsflyer Init:" + devKey); 

            AppsFlyer.setAppsFlyerKey(devKey);
            AppsFlyer.setAppID(Application.identifier);
#if UNITY_ANDROID
            AppsFlyer.init(devKey,"AppsFlyerTrackerCallbacks");
#endif
            // for getting the conversion data
            AppsFlyer.loadConversionData("StartUp");
            AppsFlyer.trackAppLaunch();

            //For Android Uninstall
            //AppsFlyer.setGCMProjectNumber ("YOUR_GCM_PROJECT_NUMBER");
            IsInitialized = true;

            LogEvent("af_open_session");
        }
        
        public override void AppsFlyerTrackerCallbacks()
        {
            LogEvent("af_android_conversion");
        }
    }
}
#endif