using UnityEngine;
using Amanotes.TripleSDK.Core;
using System.Collections.Generic;
using System.Text;

namespace  Amanotes.TripleSDK.Appsflyer
{
    
    public class AppsflyerEditorService : AppsflyerService
    {
        public override void AppsFlyerTrackerCallbacks()
        {
            LogEvent("af_editor_conversion");
        }

        public override void Init(string devKey, string ituneId)
        {
            LogEvent("af_open_session");
        }
        
        public override void LogEvent(string eventName){
           Debug.Log("[Editor] LogEvent " + eventName);
        } 

        public override void LogEvent(string eventName, Dictionary<string, object>  param) {
            var paramText = "";
            if(param != null) {
                StringBuilder builder = new StringBuilder();
                foreach (var k in param.Keys)
                {
                    builder.AppendFormat("{0}={1}, ",k, param[k]);
                }
                paramText = builder.ToString();
            }
            
             Debug.Log("[Editor] LogEvent params: " + paramText);
        } 
    }


    [InjectService(TargetPlatform = (int)RuntimePlatform.WindowsEditor, Package = "AmaAppsflyers")]
    [StartupService]
     public class AppsflyerWindowsService : AppsflyerEditorService
     {

     }

    [InjectService(TargetPlatform = (int)RuntimePlatform.OSXEditor, Package = "AmaAppsflyers")]
    [StartupService]
     public class AppsflyerMacService : AppsflyerEditorService
     {

     }
}