using UnityEngine;

namespace  Amanotes.TripleSDK.Appsflyer
{
    public class AppsflyerConfiguration: ScriptableObject
    {
        public string devKey;

        [Tooltip("iOS AppID")]
        public string appId;    // iOS

    }
}